#!/usr/local/bin/Rscript

Args <- commandArgs(TRUE)

if (length(Args) < 1) {
  cat("\nError: No input file supplied.")
  cat("\n\t Syntax: output_graphics_latest.r <input-bmrb> \n\n")
  quit()
}

#Args[1] = "bmr16117_2KDM_GB95"


## Input bmrb feature file
infile = paste(Args[1], "sspred1", sep=".")
outfile = paste(Args[1], "sspred.png", sep=".")


data = read.table(file=infile,sep=' ', header=FALSE, blank.lines.skip=TRUE, comment.char="#")
resn = data[,1]
residue = data[,2]
predSS = data[,3]
tagSS = NULL
res.per.line = 50 # number of residues per line in the output graph
xaxis_start = 0
xaxis_end = 0
bstrand_begin = 0
coil_begin = 0
helix_begin = 0
singleHelix = 0
singleCoil = 0
singleBeta = 0 
last_single_res_ss = 0

# graphical parameters used for aesthetics
coil.offset.left = 0.15
coil.offset.right = -0.15
beta.lw = 18
beta.offset.left = 0.40
beta.offset.right = -0.45
helix.lw = 8
helix.offset.left = 0.25
helix.offset.right = -0.15
helix.func = function(x) sin(x*2.8)*3 + 1

for (i in 1:length(predSS)){
  if (predSS[i] == 'B'){
    tagSS[i] = -0.5
  }
  else if (predSS[i] == 'H'){
    tagSS[i] = 0.5
  }else if (predSS[i] == 'C'){
    tagSS[i] = 0
  }

}

# n determines the splitting of the graph at every 50 residues (res.per.line)
tagSSLen = length(tagSS)
tagSS[tagSSLen+1]=1 #inserting a trailing character to facilitate displaying the last secondary structure element
n = ceiling(tagSSLen/ res.per.line)

pixels.per.row = 345
pixels.rest = 115
png(filename=outfile, width=950, height=(pixels.rest + ceiling(tagSSLen/res.per.line)*pixels.per.row) )

# if (tagSSLen < 150){
# 	bmp(filename=outfile, width=700, height=700)
# }else if (tagSSLen > 150 && tagSSLen < 200){
#         bmp(filename=outfile, width=700, height=1000)
# }else{
# 	bmp(filename=outfile, width=900, height=1500)
# }
par (mfcol=c(n+(n+1), 1), xpd=TRUE)

par(mar=c(3,3,3,3))
plot(1, type = "n", axes=FALSE, xlab="", ylab="")
legend("center", inset = 0, c("Beta-strand", "Alpha-helix", "Coil"), col=c("blue","red", "black"),lwd =7, cex =2.6, horiz=TRUE )



start = 1
res_end = start+50-1
if (res_end > tagSSLen){
     res_end = tagSSLen
}


#--------------------------------------------------------
# Drawing the graphics and barplots for secondary structures
# alpha-helix = sine curve with xaxis*2.8 period
# beta-strand = arrow
# random -coil = straight line
#--------------------------------------------------------

for (i in 1:n){
  
  #par(mar=c(3,3,3,3))
  #p= plot(c(0.7,50 ), c(1, 20), type="n", axes=FALSE, xlab="", ylab="")
  #axis(1, at=p, tick=FALSE)
  
  par(mar=c(3,0,3,0))
  p= plot(c(0,res.per.line ), c(1, 10), type="n", axes=FALSE, xlab="", ylab="")
  
  x_axis = 0
  y_axis = 0
 
  for (j in start:res_end){
    
    if ((j %% res.per.line) == 1) {
      singleCoil = 0;
      singleHelix = 0;
      singleBeta = 0;
    }
    
    if (j <= res_end){
      #cat(resn[j], tagSS[j], tagSS[j+1], '<-- residue\n')
      
      # check which SS elements are starting
      if(tagSS[j] == 0.5 && helix_begin != 1){
        xaxis_start = x_axis
        helix_begin = 1
      }else if (tagSS[j] == -0.5  && bstrand_begin != 1){
        xaxis_start =  x_axis
        bstrand_begin = 1
      }else if (tagSS[j] == 0.0 && coil_begin != 1){
        xaxis_start =  x_axis
        coil_begin = 1
      }
      
      
        
        
        
      
      # when a ss segment ends and next segment starts, then start drawing the previous one
      if (tagSS[j] != tagSS[j+1]){
        xaxis_end = x_axis
        
        #cat(" j: ", j, " res_end: ", res_end, "\n")
        
        # drawing the last single residue secondary structure
        # last residue position is indicated by the value 1 (integer)
        if (tagSS[j+1] == 1 && (tagSS[j-1] != tagSS[j] )){
          
          last_single_res_ss = 1
          xaxis_start = xaxis_end
          xaxis_end = xaxis_start + 1 	
        }
        
        
        if (tagSS[j] == 0.5 ){ #drawing the helix
          
          #cat("draw helix\n")
          #cat("xaxis_start: ", xaxis_start, "xaxis_end: ", xaxis_end, " x_axis: ", x_axis, "\n")
          
          # determining offset for drawing rectangular box or arrow when
          # there are some preceding random coils 
          
          if (singleHelix== 1){
            curve(helix.func, xlim=c(xaxis_start+helix.offset.left, xaxis_end+1+helix.offset.right), lwd=helix.lw, col="red", add=TRUE ) 	
            singleHelix = 0
          }else if (last_single_res_ss == 1 ){
            
            curve(helix.func, xlim=c(xaxis_start+helix.offset.left, xaxis_end+1+helix.offset.right), lwd=helix.lw, col="red", add=TRUE )
            last_single_res_ss = 0
            
            
          }else{
            curve(helix.func, xlim=c(xaxis_start+helix.offset.left, xaxis_end+1+helix.offset.right), lwd=helix.lw, col="red", add=TRUE )
          } 
          helix_begin = 0
        }else if(tagSS[j] == -0.5 ){# drawing the beta strand
          if (last_single_res_ss == 1 ){
            arrows(xaxis_start+beta.offset.left, 0, xaxis_end+1+beta.offset.right, 0, length=0.25, angle=30, lwd=beta.lw, col="blue")
            last_single_res_ss = 0
          }else  if (singleBeta == 1){
            arrows(xaxis_start+beta.offset.left, 0, xaxis_end+1+beta.offset.right, 0, length=0.25, angle=30, lwd=beta.lw, col="blue")
            singleBeta = 0
          }else{
            if (xaxis_start == xaxis_end) {
              arrows(-0.4, 0, xaxis_end+1+beta.offset.right, 0, length=0.25, angle=30, lwd=beta.lw, col="blue")
            } else {
              arrows(xaxis_start+beta.offset.left, 0, xaxis_end+1+beta.offset.right, 0, length=0.25, angle=30, lwd=beta.lw, col="blue")
            }
          }
          
          bstrand_begin = 0
        }else if(tagSS[j] == 0.0 ){ # drawing the coil residue
          if (singleCoil== 1){
            segments(xaxis_start+coil.offset.left, 0,xaxis_start+1+coil.offset.right, 0, lwd=4, col="black")
            singleCoil = 0
          }else if (last_single_res_ss == 1 ){
            segments(xaxis_start+coil.offset.left, 0,xaxis_start+1+coil.offset.right, 0, lwd=4, col="black")
            last_single_res_ss  = 0
          }else{
            segments(xaxis_start+coil.offset.left, 0,xaxis_end+1+coil.offset.right, 0, lwd=4, col="black")
          }
          coil_begin = 0
        }
        
        
        
      }else if(tagSS[j] == tagSS[j+1] ){
        
        
        # if a ss segment reaches at the end of each row, then draw the structure uptpo this point
        if (j == res_end){
          xaxis_end = x_axis
          if (tagSS[j] == 0.5){
            curve(helix.func, xlim=c(xaxis_start+helix.offset.left, xaxis_end+1+helix.offset.right), lwd=helix.lw, col="red", add=TRUE )
            helix_begin = 0
          }else if (tagSS[j] == -0.5){
            segments(xaxis_start+beta.offset.left, 0, xaxis_end+1+beta.offset.right, 0, lwd=beta.lw, col="blue")
            bstrand_begin = 0
          }else if (tagSS[j] == 0){
            
            segments(xaxis_start+coil.offset.left, 0,xaxis_end+1+coil.offset.right, 0, lwd=4, col="black")
            coil_begin = 0
          }
          
        }
      }  
      
    }
    x_axis = x_axis +1
  }
  
  
  
#===============================================================================================================================
  #cat(tagSS) 
#   for (j in start:res_end){
#     
#    if (tagSS[j+1]== 1 && j == start+1 && (tagSS[j-1] == tagSS[j])){
#       #cat("Drawing the last residue in  a new row......\n")
#       last_single_res_ss = 1
#       xaxis_start = xaxis_end
#       xaxis_end = xaxis_start + 1
#     }
# 
# 
#     x_axis = x_axis +1
# 
#     if (j <= res_end){
# 
#       if (tagSS[j] != tagSS[j+1]){
#          xaxis_end = x_axis
#        	
#         if ( j+1 == res_end){
#           if (tagSS [j+1] == 0.0){
# 	 	          segments(x_axis, 0,x_axis+1, 0, lwd=4)
#           }else if (tagSS [j+1] == -0.5){
#               arrows(xaxis_start+beta.offset.left, 0, xaxis_end+1+beta.offset.right, 0, length=0.25, angle=30, lwd=beta.lw, col="blue")
#        	  }else if (tagSS [j+1] == 0.5){
#               curve(helix.func, xlim=c(xaxis_start+helix.offset.left, xaxis_end+1+helix.offset.right), lwd=helix.lw, col="red", add=TRUE)
# 
#          }
#        }
#  
#        # drawing the last residue secondary structure
#        if (tagSS[j+1] == 1 && (tagSS[j-1] != tagSS[j])){
# 	       last_single_res_ss = 1
# 	        xaxis_start = xaxis_end
#  	        xaxis_end = xaxis_start + 1 	
#        }
# 	      if ( j+1 != res_end && helix_begin == 0 && coil_begin == 0 && bstrand_begin == 0){
#          	#print(":::::::::::::::::")
#         	if (tagSS[j+1] == 0.0) {singleCoil = 1}
# 		      else if (tagSS[j+1] == 0.5) {singleHelix = 1}
# 		      else if (tagSS[j+1] == -0.5) {singleBeta = 1}
#          	xaxis_start = x_axis
#          	xaxis_end = xaxis_start + 1
#         }	
#         if (tagSS[j] == 0.5 ){ #drawing the helix
# 
#         	# determining offset for drawing rectangular box or arrow when
#        	 	# there are some preceding random coils 
# 		      
# 		      if (singleHelix== 1){
#            	  curve(helix.func, xlim=c(xaxis_start+helix.offset.left, xaxis_end+1+helix.offset.right), lwd=helix.lw, col="red", add=TRUE ) 	
#            		singleHelix = 0   		
# 		      }else if (last_single_res_ss == 1){
#                curve(helix.func, xlim=c(xaxis_start+helix.offset.left, xaxis_end+1+helix.offset.right), lwd=helix.lw, col="red", add=TRUE )
#                last_single_res_ss = 0
#           }
#           else{
# 			     	  curve(helix.func, xlim=c(xaxis_start+helix.offset.left, xaxis_end+1+helix.offset.right), lwd=helix.lw, col="red", add=TRUE )
# 		      } 
# 	 	      helix_begin = 0
#         }else if(tagSS[j] == -0.5 ){#drawing the beta-strand
#             if (last_single_res_ss == 1){
#         		    arrows(xaxis_start+beta.offset.left, 0, xaxis_end+1+beta.offset.right, 0, length=0.25, angle=30, lwd=beta.lw, col="blue")
# 			          last_single_res_ss = 0
#         	  }else  if (singleBeta == 1){
#                 arrows(xaxis_start+beta.offset.left, 0, xaxis_end+1+beta.offset.right, 0, length=0.25, angle=30, lwd=beta.lw, col="blue")
#                 singleBeta = 0
#             }
#             else{
# 			          arrows(xaxis_start+beta.offset.left, 0, xaxis_end+1+beta.offset.right, 0, length=0.25, angle=30, lwd=beta.lw, col="blue")
# 		        }
#             bstrand_begin = 0
#         }else if(tagSS[j] == 0.0 ){#drawing the coil
# 		        if (singleCoil== 1){
#                 segments(xaxis_start, 0,xaxis_end, 0, lwd=4)
# 			          singleCoil = 0
# 		        }else if (last_single_res_ss == 1){
#                 segments(xaxis_start, 0,xaxis_end, 0, lwd=4)
#                 last_single_res_ss  = 0
#             } 
#             else{
# 			          segments(xaxis_start, 0,xaxis_end, 0, lwd=4)
# 		        }
#         	  coil_begin = 0
#         }
# 
#     }else if(tagSS[j] == tagSS[j+1] ){
#       
#       if (j+1 == res_end){
# 	        xaxis_end = x_axis
#           if (tagSS[j] == 0.5){
# 		            curve(helix.func, xlim=c(xaxis_start+helix.offset.left, xaxis_end+1+helix.offset.right), lwd=helix.lw, col="red", add=TRUE )
#                 helix_begin = 0
# 	        }else if (tagSS[j] == -0.5){
#                 arrows(xaxis_start+beta.offset.left, 0, xaxis_end+1+beta.offset.right, 0, length=0.25, angle=30, lwd=beta.lw, col="blue")
#                 bstrand_begin = 0
#            }else if (tagSS[j] == 0){
# 		            segments(xaxis_start, 0,xaxis_end, 0, lwd=4)
#                 coil_begin = 0
# 	         }
# 
#       }else{	
#  	         if(tagSS[j] == 0.5 && helix_begin != 1){
#         	      xaxis_start = x_axis
#         	  		helix_begin = 1
# 
#       	   }else if (tagSS[j] == -0.5 && bstrand_begin != 1){
#         	      xaxis_start =  x_axis
#         	      bstrand_begin = 1
# 
#       	   }else if (tagSS[j] == 0.0 && coil_begin != 1){
#         	      xaxis_start =  x_axis
#         	      coil_begin = 1
#           }
#   	   }
# 	  }  
# 	
#    }
# }


#===============================================================================================================================


#---------------------------------------------
  # drawing the barplot and residue numbering 
  #---------------------------------------------





  #par(mar=c(3,3,3,3))
  par(mar=c(2, 0, 2, 0))
  currentTagSS = tagSS[start:res_end]
  currentTagSSLen=length(currentTagSS)

  #---------------------------------------------
  # drawing the barplot and residue numbering 
  #---------------------------------------------

  labelfreq = 10
  if (currentTagSSLen < res.per.line){
    # if the last vector of residues has less than 50 entries then
    # fill up the rest of the positions with white bars without border
    interval = 1/(res.per.line -currentTagSSLen)
    newItems = seq(-0.6, 0.4, by=interval)
    currentTagSS=c(currentTagSS,newItems)
    #col= rgb(1, 0, 0,0.5) --> the 4th argument is for transparency
    d= barplot(currentTagSS, ylim=c(-1,1), col=ifelse(currentTagSS==0.5,"red", ifelse(currentTagSS==-0.5,"blue", ifelse(currentTagSS==0.0,"black", "white"))),axes=FALSE, xaxt='n', border=ifelse((currentTagSS==0.5|currentTagSS==-0.5 |currentTagSS==0.0 ),TRUE,NA ))
    
    if (currentTagSSLen < labelfreq) {
        at.vals = d[c(1, currentTagSSLen)]
        label.vals = resn[c(start, resn[length(resn)])]
    } else {
        at.vals = d[c(1,seq(labelfreq, currentTagSSLen, labelfreq), currentTagSSLen)]
        label.vals = resn[c(start,seq(start+labelfreq-1, res_end, labelfreq), resn[length(resn)])]
    } 
    axis(side=3,at=at.vals, labels=label.vals, line=-0.5, lwd=0.0, cex.axis =1.8)

    #text(-2,0, labels=c(resn[start]), cex=1.8)
    #text(currentTagSSLen+12,0, labels=c(resn[res_end]), cex =1.8)
    #mtext (start, side=2)
    #mtext (res_end, side=4)
  }

  else{
    d= barplot(currentTagSS, ylim=c(-1,1), col=ifelse(currentTagSS==0.5,"red", ifelse(currentTagSS==-0.5,"blue", "black")),axes=FALSE, xaxt='n', border=TRUE )
    #mtext (start, side=2, padj = 0)
    #mtext (res_end, side=4, padj = 0)
 
     axis(side=3,at=d[c(1,seq(labelfreq, res.per.line, labelfreq))], labels=resn[c(start,seq(start+labelfreq-1, res_end, labelfreq))], line=-0.5, lwd=0, cex.axis =1.8)

    #text(-2,0, labels=c(resn[start]), cex=1.8)  # printing the start residue label
    #text(50+12,0, labels=c(resn[res_end]), cex =1.8) # printing the end resiue label
  }
  #axis(1, c(1,limit) , tick = FALSE, labels=FALSE)

  start = res_end+1
  if (start+res.per.line-1 > tagSSLen){
    res_end = tagSSLen
    #print(start)
    #print(res_end)
  }else{
    res_end = start+res.per.line-1
  }

}

dev.off()
                                                                                        
