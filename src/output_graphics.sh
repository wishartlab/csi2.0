#!/bin/bash

echo $DISPLAY

if [[ -z "$DISPLAY" ]]; then 
	export DISPLAY=:1
fi
bmrbid=$1
###################################################
# produce graphics of predicted secondary structure
###################################################
/usr/local/bin/Rscript /apps/csi/project/Standalone_CSI2.0/src/output_graphics.r $bmrbid
