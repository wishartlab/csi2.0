#/usr/bin/python

#==============================================================
## This python script parses a bmrb 3.1 format and convets into 
## a talos formatted file
## sequence in bmrb3.1 format
#loop_
      #_Entity_comp_index.ID
      #_Entity_comp_index.Auth_seq_ID
      #_Entity_comp_index.Comp_ID
      #_Entity_comp_index.Comp_label
      #_Entity_comp_index.Entry_ID
      #_Entity_comp_index.Entity_ID

       #1 . MET . 4403 1 
       #2 . ASP . 4403 1 
       #3 . ARG . 4403 1 
       #4 . VAL . 4403 1 
       #5 . LEU . 4403 1 
       #6 . SER . 4403 1 
       #7 . ARG . 4403 1 
       #8 . ALA . 4403 1 
       #9 . ASP . 4403 1 
      #10 . LYS . 4403 1 
      #11 . GLU . 4403 1 
      #12 . ARG . 4403 1 

   #stop_

## chemical shift header and entry in bmrb3.1 format
      #_Atom_chem_shift.Auth_comp_ID
      #_Atom_chem_shift.Auth_atom_ID
      #_Atom_chem_shift.Details
      #_Atom_chem_shift.Entry_ID
      #_Atom_chem_shift.Assigned_chem_shift_list_ID

        #1 . 1 1  1  1 CYS HA   H 1 4.18 . . 1 . . . . . . . . 1503 1 
        #2 . 1 1  1  1 CYS HB2  H 1 3    . . 2 . . . . . . . . 1503 1 
        #3 . 1 1  1  1 CYS HB3  H 1 3.12 . . 2 . . . . . . . . 1503 1 
        #4 . 1 1  2  2 SER H    H 1 8.78 . . 1 . . . . . . . . 1503 1 
        #5 . 1 1  2  2 SER HA   H 1 4.53 . . 1 . . . . . . . . 1503 1 
        #6 . 1 1  2  2 SER HB2  H 1 3.6  . . 1 . . . . . . . . 1503 1 
        #7 . 1 1  2  2 SER HB3  H 1 3.6  . . 1 . . . . . . . . 1503 1 
        #8 . 1 1  3  3 CYS H    H 1 8.18 . . 1 . . . . . . . . 1503 1 
        #9 . 1 1  3  3 CYS HA   H 1 4.75 . . 1 . . . . . . . . 1503 1 
       #10 . 1 1  3  3 CYS HB2  H 1 2.83 . . 2 . . . . . . . . 1503 1 
       #11 . 1 1  3  3 CYS HB3  H 1 3.14 . . 2 . . . . . . . . 1503 1 
       #12 . 1 1  4  4 SER H    H 1 8.39 . . 1 . . . . . . . . 1503 1 
       #13 . 1 1  4  4 SER HA   H 1 4.42 . . 1 . . . . . . . . 1503 1
       
#==============================================================
## import libraries
import sys
from collections import defaultdict

## variable definition

three2one ={'ALA' : 'A','ARG' : 'R','ASN' : 'N','ASP' : 'D','ASX' : 'B','CYS' : 'C', 
             'GLN' : 'Q','GLU' : 'E','GLX' : 'Z','GLY' : 'G','HIS' : 'H','ILE' : 'I','LEU' : 'L','LYS' : 'K','MET' : 'M','PHE' : 'F','PRO' : 'P','SER' : 'S','THR' : 'T','TRP' : 'W','TYR' : 'Y','VAL' : 'V','PCA' : 'X'}

atoms=["HA", "H","N", "CA", "CB", "C", "HA2", "HA3" ] # For GLY, HA2 and HA3 atoms are listed

seq_id_list=[]
seq_num_list = []
comp_id_list = []
comp_dict = defaultdict(list)

seq_start_trigger = "_Entity_comp_index.ID"
chemshift_start_trigger = "_Atom_chem_shift.Assigned_chem_shift_list_ID"
stop_trigger = "stop_"

## sequence and chemical shift parsing
def parse_bmrb_3_file(fin):
    seq_entry_done = 0
    seq_start = 0
    chem_shift_start = 0

    for line in fin:
        if not line.strip(): continue
        line = line.lstrip()
        if not seq_entry_done and seq_start_trigger in line:
            seq_start = 1
           
        if seq_start and not line.startswith(stop_trigger):
            elems = line.split()
            if len(elems) == 6 and len(elems[2])== 3:
                seq_num_list.append(elems[0])
                comp_id_list.append(elems[2])
                
        if seq_start and line.startswith(stop_trigger):        
            seq_start = 0
            seq_entry_done = 1
        ##chemical shift entry parsing starts    
        if line.startswith(chemshift_start_trigger ):
            chem_shift_start = 1
        if chem_shift_start and len(line.split())== 24 and len(line.split()[6])== 3:
            elems = line.split()
            seq_id = elems[5]
            if seq_id not in seq_id_list:
                seq_id_list.append(seq_id)
            comp_id = elems[6]
            atom_id = elems[7]
            if atom_id in atoms:
                if atom_id == "H": atom_id = "HN"
                chem_shift_val = elems[10]
                comp_dict[seq_id].append([comp_id, atom_id, chem_shift_val])
        if chem_shift_start and line.startswith(stop_trigger):
            chem_shift_start= 0
            break #done parsing
        
## creating subsequence each length 10 for DATA SEQUENCE header 

def print_talos_format():
    for n in range(0,len(seq_num_list),5):
        
        if n+5 < len(seq_num_list):
            end = n+5
        else:
            end = len(seq_num_list)
        print("SEQ", end =" ")
        for m in range(n,end):
            print("%s"%(seq_num_list[m]+comp_id_list[m]), end = " ")
        print("\n", end="")    
        
    
    print("\nVARS RESID RESNAME ATOMNAME SHIFT")
    print("FORMAT %4d %4s %4s %8.3f\n")
    for i in range(len(seq_id_list)):
        seq_id = seq_id_list[i]
        val = comp_dict[seq_id_list[i]]
        
        for j in range(len(val)):
            entry = " ".join(val[j])
            print("%s %s"%(seq_id, entry))
        
        
## read file name and open file
bmrb3_1_file = sys.argv[1]
fin = open(bmrb3_1_file, "r")
parse_bmrb_3_file(fin)
print_talos_format()
    
