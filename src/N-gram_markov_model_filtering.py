#!/usr/local/bin/python3
from os.path import isfile, join
from os import listdir
import sys

three2one ={'ALA' : 'A','ARG' : 'R','ASN' : 'N','ASP' : 'D','ASX' : 'B','CYS' : 'C', 
             'GLN' : 'Q','GLU' : 'E','GLX' : 'Z','GLY' : 'G','HIS' : 'H','ILE' : 'I','LEU' : 'L','LYS' : 'K','MET' : 'M','PHE' : 'F','PRO' : 'P','SER' : 'S','THR' : 'T','TRP' : 'W','TYR' : 'Y','VAL' : 'V','PCA' : 'X'}

def combination(window_size):
    SSAlphabet = ['B', 'C', 'H']
    substr_db = []
    substr_db_ws_5 = []# substring database of window size = 5
    substr_db_ws_4 = []
    substr_db_ws_3 = []
    substr_db_ws_2 = []
    substr_db_ws_1 = ['B','C','H']
    
    # finding all possible substrings of window_size consists of 3 possible letters
    # each position of the substring has 3 possibilities ('B','C','H')
    substr = window_size*['_']
    '''
    for i in range(window_size):
        
        for j in range(len(SSAlphabet)):
            substr[i] = SSAlphabet[j]
            for l in range(len(SSAlphabet)):
                if l!=j:
                    for k in range(window_size):
                        if k!= i:
                            substr[k] = SSAlphabet[l]
                    print(substr)        
                    substr_db.append(substr)
            substr = 5*['_']        
        '''
    if window_size==7:
        i = 0
        for j in range(len(SSAlphabet)):
            substr[i] = SSAlphabet[j]
            for k in range(len(SSAlphabet)):
                substr[i+window_size-6] = SSAlphabet[k]
                for l in range(len(SSAlphabet)):
                    substr[i+window_size-5] = SSAlphabet[l]
                    for m in range(len(SSAlphabet)):
                        substr[i+window_size-4] = SSAlphabet[m]
                        for n in range(len(SSAlphabet)):
                            substr[i+window_size-3] = SSAlphabet[n]    
                            for o in range(len(SSAlphabet)):
                                substr[i+window_size-2] = SSAlphabet[o]
                                for p in range(len(SSAlphabet)):
                                    substr[i+window_size-1] = SSAlphabet[p]
                                    substr_db.append("".join(substr))
        substr = window_size*['_']
    if window_size==6:
        i = 0
        for j in range(len(SSAlphabet)):
            substr[i] = SSAlphabet[j]
            for k in range(len(SSAlphabet)):
                substr[i+window_size-5] = SSAlphabet[k]
                for l in range(len(SSAlphabet)):
                    substr[i+window_size-4] = SSAlphabet[l]
                    for m in range(len(SSAlphabet)):
                        substr[i+window_size-3] = SSAlphabet[m]
                        for n in range(len(SSAlphabet)):
                            substr[i+window_size-2] = SSAlphabet[n]    
                            for o in range(len(SSAlphabet)):
                                substr[i+window_size-1] = SSAlphabet[o]
                                substr_db.append("".join(substr))
        substr = window_size*['_']
    if window_size==5:
        i = 0
        for j in range(len(SSAlphabet)):
            substr[i] = SSAlphabet[j]
            for k in range(len(SSAlphabet)):
                substr[i+window_size-4] = SSAlphabet[k]
                for l in range(len(SSAlphabet)):
                    substr[i+window_size-3] = SSAlphabet[l]
                    for m in range(len(SSAlphabet)):
                        substr[i+window_size-2] = SSAlphabet[m]
                        for n in range(len(SSAlphabet)):
                            substr[i+window_size-1] = SSAlphabet[n]    
                            substr_db.append("".join(substr))
        substr = window_size*['_']        
    if window_size==4:
        i = 0
        for j in range(len(SSAlphabet)):
            substr[i] = SSAlphabet[j]
            for k in range(len(SSAlphabet)):
                substr[i+window_size-3] = SSAlphabet[k]
                for l in range(len(SSAlphabet)):
                    substr[i+window_size-2] = SSAlphabet[l]
                    for m in range(len(SSAlphabet)):
                        substr[i+window_size-1] = SSAlphabet[m]
                        #print(substr)        
                        #substr_db_ws_4.append(substr)
                        substr_db.append("".join(substr))
        substr = window_size*['_']                
    if window_size==3:
        i = 0
        for j in range(len(SSAlphabet)):
            substr[i] = SSAlphabet[j]
            for k in range(len(SSAlphabet)):
                substr[i+window_size-2] = SSAlphabet[k]
                for l in range(len(SSAlphabet)):
                    substr[i+window_size-1] = SSAlphabet[l]
                    #print(substr)        
                    #substr_db_ws_3.append(substr)
                    substr_db.append("".join(substr))
        substr = window_size*['_']
    if window_size==2:
        i = 0
        for j in range(len(SSAlphabet)):
            substr[i] = SSAlphabet[j]
            for k in range(len(SSAlphabet)):
                substr[i+window_size-1] = SSAlphabet[k]
                #print(substr)        
                #substr_db_ws_2.append(substr)
                substr_db.append("".join(substr))
        substr = window_size*['_'] 
    #print(len(substr_db))    
    return substr_db  
     
def read_SSstring_db():
    

    fin = open("/apps/csi/project/Standalone_CSI2.0/data/TrainSSDB", "r")
    SSstring_db = []
    for line in fin:
        if line.strip():
            SSstring=line.rstrip()
            SSstring_db.append(SSstring)
    fin.close()
    return SSstring_db
        
def count_frequency(SSstring_db):
    substr_db_ws_7= combination(7)
    substr_db_ws_6= combination(6)
    substr_db_ws_5= combination(5)
    substr_db_ws_4= combination(4)
    substr_db_ws_3= combination(3)
    substr_db_ws_2= combination(2)
    SSstring_dict = dict()
    for SSstring in SSstring_db:
        SSlist = list(SSstring)
        #7-letter SS string frequency count
        for i in range(len(SSlist)):
            substr=''
            if (i + 6) > len(SSlist):
                substr = "".join(SSlist[i:len(SSlist)])
            elif (i + 6) < len(SSlist):    
                for j in range(i, i+7):
                    substr += SSlist[j]
            if substr in SSstring_dict:
                SSstring_dict[substr]+=1
            else:
                SSstring_dict[substr]= 1
        #6-letter SS string frequency count
        for i in range(len(SSlist)):
            substr=''
            if (i + 5) > len(SSlist):
                substr = "".join(SSlist[i:len(SSlist)])
            elif (i + 5) < len(SSlist):    
                for j in range(i, i+6):
                    substr += SSlist[j]
            if substr in SSstring_dict:
                SSstring_dict[substr]+=1
            else:
                SSstring_dict[substr]= 1
        #5-letter SS string frequency count
        for i in range(len(SSlist)):
            substr=''
            if (i + 4) > len(SSlist):
                substr = "".join(SSlist[i:len(SSlist)])
            elif (i + 4) < len(SSlist):    
                for j in range(i, i+5):
                    substr += SSlist[j]
            if substr in SSstring_dict:
                SSstring_dict[substr]+=1
            else:
                SSstring_dict[substr]= 1
            
                
                
        #4-letter SS string frequency count        
        for i in range(len(SSlist)):
            substr=''
            if (i + 3) > len(SSlist):
                substr = "".join(SSlist[i:len(SSlist)])
            elif (i + 3) < len(SSlist):    
                for j in range(i, i+4):
                    substr += SSlist[j]
            if substr in SSstring_dict:
                SSstring_dict[substr]+=1
            else:
                SSstring_dict[substr]= 1
        #3-letter SS string frequency count        
        for i in range(len(SSlist)):
            substr=''
            if (i + 2) > len(SSlist):
                substr = "".join(SSlist[i:len(SSlist)])
            elif (i + 2) < len(SSlist):    
                for j in range(i, i+3):
                    substr += SSlist[j]
            if substr in SSstring_dict:
                SSstring_dict[substr]+=1
            else:
                SSstring_dict[substr]= 1 
        #2-letter SS string frequency count        
        for i in range(len(SSlist)):
            substr=''
            if (i + 1) > len(SSlist):
                substr = "".join(SSlist[i:len(SSlist)])
            elif (i + 1) < len(SSlist):    
                for j in range(i, i+2):
                    substr += SSlist[j]
            if substr in SSstring_dict:
                SSstring_dict[substr]+=1
            else:
                SSstring_dict[substr]= 1
        #1-letter SS string frequency count        
        for i in range(len(SSlist)):
            
            if SSlist[i] in SSstring_dict:
                SSstring_dict[SSlist[i]]+=1
            else:
                SSstring_dict[SSlist[i]]= 1
                
        # filling up the empty slots in the dictionary
        for substr in substr_db_ws_7:
            if substr not in SSstring_dict:
                SSstring_dict[substr]= 1
        for substr in substr_db_ws_6:
            if substr not in SSstring_dict:
                SSstring_dict[substr]= 1
        for substr in substr_db_ws_5:
            if substr not in SSstring_dict:
                SSstring_dict[substr]= 1
        for substr in substr_db_ws_4:
            if substr not in SSstring_dict:
                SSstring_dict[substr]= 1
        for substr in substr_db_ws_3:
            if substr not in SSstring_dict:
                SSstring_dict[substr]= 1
        for substr in substr_db_ws_2:
            if substr not in SSstring_dict:
                SSstring_dict[substr]= 1
    return SSstring_dict        
                

def main():
    bmrbid = sys.argv[1] 
    window_size = int(sys.argv[2])
    inFile = bmrbid+".sspred1"


    SSstring_db=read_SSstring_db()
    SSstring_dict=count_frequency(SSstring_db)  
    
    
    
    
    predSSstring = []
    AAstring = []
    resnList = [] 	
    fin = open(inFile, "r")
    for line in fin:
        if line.strip() and not line.startswith("#"):
            resnList.append(int(line.split()[0]))
            AAstring.append(line.split()[1])
            predSSstring.append(line.rstrip().split()[2])
            
            
    fin.close()  
    if window_size == 7:   
        substr_no = 1
        posterior_prob_dict = dict()
        posterior_prob_list = []
        substr_list = []
        substr_dict = dict()
        for i in range(len(predSSstring)):
            substr=''
            if (i + 7) > len(predSSstring):
                substr = "".join(predSSstring[i:len(predSSstring)])
                #print(substr)
            if (i + 7) < len(predSSstring):    
                for j in range(i, i+7):
                    substr += predSSstring[j]
                #print(substr) 
                    
            substr1= substr[0:len(substr)]
            substr2= substr[0:len(substr)-1]
            substr3= substr[0:len(substr)-2]
            substr4= substr[0:len(substr)-3]
            substr5= substr[0:len(substr)-4]
            substr6= substr[0:len(substr)-5]
            
            # calculating the posterior probability in 7-gram markov model
            posterior_prob = (SSstring_dict[substr]/ SSstring_dict[substr1]) * (SSstring_dict[substr1]/ SSstring_dict[substr2])*(SSstring_dict[substr2]/ SSstring_dict[substr3]) *(SSstring_dict[substr3]/ SSstring_dict[substr4]) * (SSstring_dict[substr4]/ SSstring_dict[substr5])*(SSstring_dict[substr5]/ SSstring_dict[substr6])*SSstring_dict[substr6]
            
            substr_list.append(substr)
            posterior_prob_list.append(posterior_prob)
            posterior_prob_dict[substr] = posterior_prob
            substr_dict [substr_no] = substr
            substr_no+=1
    if window_size == 5: 
        substr_no = 1
        substr_dict = dict()
        posterior_prob_dict = dict()
        posterior_prob_list = []
        substr_list = []
        for i in range(len(predSSstring)):
            substr=''
            #if (i + 4) > len(predSSstring):
             #   substr = "".join(predSSstring[i:len(predSSstring)])
            if (i + 4) < len(predSSstring):    
                for j in range(i, i+5):
                    substr += predSSstring[j]
                    
                    
            substr1= substr[0:len(substr)]
            substr2= substr[0:len(substr)-1]
            substr3= substr[0:len(substr)-2]
            substr4= substr[0:len(substr)-3]
            
            # calculating the posterior probability in 5-gram markov model
            posterior_prob = (SSstring_dict[substr]/ SSstring_dict[substr1]) * (SSstring_dict[substr1]/ SSstring_dict[substr2])*(SSstring_dict[substr2]/ SSstring_dict[substr3]) *(SSstring_dict[substr3]/ SSstring_dict[substr4]) * SSstring_dict[substr4]
            
            substr_list.append(substr)
            posterior_prob_list.append(posterior_prob)
            posterior_prob_dict[substr] = posterior_prob
            substr_dict [substr_no] = substr
            substr_no+=1
            
    maximum = max(posterior_prob_list)         
    minimum = min(posterior_prob_list)
    posterior_norm_prob_dict = dict()
    for substr in substr_list:
        prob = posterior_prob_dict[substr]
        norm_prob = (prob - minimum)/(maximum-minimum)
        posterior_norm_prob_dict[substr] = "%0.6f" %(norm_prob)
        #print("%s\t\t%s" %(substr, posterior_norm_prob_dict[substr]))
        
   
    filteredSSstring= list("".join(predSSstring))
    
    
    ## filtering the initial prediction based on posterior probability of mhaving seven-residue tokens along the protein chain 
    for i in range(1, substr_no):
        substr = substr_dict[i]
        substrList = []  
        current = posterior_norm_prob_dict[substr]
        # the last substring    	 
        if (len(substr) == 6 and posterior_norm_prob_dict[substr] == "0.000000"):
            #print(substr, posterior_norm_prob_dict[substr])
             for k in range(0, len(substr)-1):
                if substr[k]!=substr[k-1] and substr[k]!=substr[k+1]:
                    filteredSSstring[i-1+k] = filteredSSstring[i+k]

              
        if i == 1:
            for j in range(i, i+3):
                if posterior_norm_prob_dict[substr_dict[j]] == "0.000000":
                    #print("..........",substr_dict[j])
                    substrList.append(substr_dict[j])
        if i > 2 and i < substr_no-2:
            for j in range(i-2, i+3):
                if posterior_norm_prob_dict[substr_dict[j]] == "0.000000" or posterior_norm_prob_dict[substr_dict[j]].startswith("0.000"):
                    #print("::::::::::",substr_dict[j])
                    substrList.append(substr_dict[j])
        if substrList: 
            ##for N- terminus region
            #if i == 1 and  ( len(substrList) == 2 or len(substrList) == 3 ):
                
                #substr2modify = substrList[1]
                #if substr2modify[1] == substr2modify[2] and substr2modify[2] == substr2modify[4]:
                        #filteredSSstring[i-1+3] = predSSstring[i-1+4]
                #elif substr2modify[4] == substr2modify[5] and substr2modify[5] == substr2modify[2]:
                        #filteredSSstring[i-1+3] = predSSstring[i-1+2]
                
                        
            if len(substrList) == 5:
                
                #for k in range(1, 6, 5):
                substr2modify = substrList[2]
                # =========================================================================================
                # original -> filtered
                # BBCBBCC -> BBBBBCC -- if k is the index, k-1, k- and k+1 to check to change the SS state 
                # BBBCBCC -> BBBBBCC
                # HHHCHCC -> HHHHHCC
                # =========================================================================================
                if len(substr2modify) >= 6:
                    #print(substr2modify, '........') #debugging code
                    for k in range(2, 5):
                        #print(substr2modify[k-2], substr2modify[k-1], substr2modify[k+1])#debugging code
                        if (substr2modify[k-2] == substr2modify[k-1]) and (substr2modify[k-2] == substr2modify[k+1]) and (substr2modify[k-2] == substr2modify[k+1]):
                           filteredSSstring[i-1+k] = filteredSSstring[i-1+k+1]
                        elif substr2modify[k+1] == substr2modify[k+2] and substr2modify[k+2] == substr2modify[k-1]:
                           filteredSSstring[i-1+k] = filteredSSstring[i-1+k+2] 
                    
    tokens_to_reject = ["BBBHBBB", "BBHHBBB", "BBBHHBB", "HHHBHHH", "HHBBHHH", "HHHBBHH", "CCHCCCC", "CHCCCCC", "CCCHCCC", "CCBCCCC", "CBCCCCC", "CCCBCCC", "CCCHHCC", "CCHHCCC" ]
    # what about the other 4 tokens ?? Are they allowed ??
    # "CCCBBCC", "CCBBCCC", ,"CCCHHCC", "CCHHCCC",
    
    for n in range(len(filteredSSstring)):
        substr=''
            
        if (n + 6) < len(filteredSSstring):    
            for m in range(n, n+7):
                substr += filteredSSstring[m]

        if substr == "BBBHBBB" or substr == "HHHBHHH" :
            filteredSSstring[n+3] = 'C'
        elif substr == "BBHHBBB" or substr == "HHBBHHH":
            filteredSSstring[n+2] = 'C'
            filteredSSstring[n+3] = 'C'
        elif substr == "BBBHHBB" or substr == "HHHBBHH":
            filteredSSstring[n+3] = 'C'
            filteredSSstring[n+4] = 'C'
        elif substr == "BHHBBBB" or substr == "HBBHHHH":
            filteredSSstring[n+1] = 'C'
            filteredSSstring[n+2] = 'C'
        elif substr == "BBBBHHB" or substr == "HHHHBBH":
            filteredSSstring[n+4] = 'C'
            filteredSSstring[n+5] = 'C'    
        elif substr == "CCHCCCC" or substr == "CCBCCCC":
            filteredSSstring[n+2] = 'C' 
        elif substr == "CCCHCCC" or substr == "CCCBCCC":
            filteredSSstring[n+3] = 'C'
        elif substr == "CHCCCCC" or substr == "CBCCCCC":
            filteredSSstring[n+1] = 'C'  
        elif substr == "CCCHHCC":
            filteredSSstring[n+3] = 'C'
            filteredSSstring[n+4] = 'C'
        elif substr == "CCHHCCC":
            filteredSSstring[n+2] = 'C'
            filteredSSstring[n+3] = 'C'  
            
    #print("Initial Prediction: %s" %"".join(predSSstring))
    #print("Filtered Prediction:%s" %"".join(filteredSSstring))
    
    
    # printiing in horizontal format
    output = ''	
    proteinLen = len(resnList)
    for i in range(0,len(resnList), 50):
        start = i
        end = start + 50-1
        if end > proteinLen:
            end = proteinLen-1 
        
        output+="%d\t%s\t%d\n" %(resnList[start], "".join(AAstring[start:end+1]), resnList[end])
        output+="%d\t%s\t%d\n" %(resnList[start], "".join(filteredSSstring[start:end+1]), resnList[end])
        output+='\n'
    finalpredfile = bmrbid + '.finalpred'
    with open(finalpredfile, "w") as fout:
        fout.write(output)    
    print("#Resn Residue PredictedSS\n")

    for i in range(0, len(resnList)):
        print ("%d %s %s" %(resnList[i], AAstring[i], filteredSSstring[i]))
	                   
            
main()    
