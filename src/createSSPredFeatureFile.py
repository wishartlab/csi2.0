#!/usr/local/bin/python3

import operator
from collections import *
import os
from os.path import isfile
from sys import argv
from os import listdir

resmap = {'A' : 'ALA','R' : 'ARG','N' : 'ASN','D' : 'ASP','B' : 'ASX','C' : 'CYS', 'a':'CYS', 'b':'CYS', 'c':'CYS', 'Q' : 'GLN','E' : 'GLU','Z' : 'GLX','G' : 'GLY','H' : 'HIS','I' : 'ILE','L' : 'LEU','K' : 'LYS','M' : 'MET','F' : 'PHE','P' : 'PRO','S' : 'SER','T' : 'THR','W' : 'TRP','Y' : 'TYR','V' : 'VAL','X' : 'PCA'}
three2one ={'ALA' : 'A','ARG' : 'R','ASN' : 'N','ASP' : 'D','ASX' : 'B','CYS' : 'C','GLN' : 'Q','GLU' : 'E','GLX' : 'Z','GLY' : 'G','HIS' : 'H','ILE' : 'I','LEU' : 'L','LYS' : 'K','MET' : 'M','PHE' : 'F','PRO' : 'P','SER' : 'S','THR' : 'T','TRP' : 'W','TYR' : 'Y','VAL' : 'V','PCA' : 'X'}


# training set mean and std. dev of features along with shannon entropy conservation score 
mean= {'ProbBCS(i-2)':0.33845057,'ProbBCS(i-1)':0.33779056,'ProbBCS(i)':0.33810181, 'ProbBCS(i+1)':0.33753342,'ProbBCS(i+2)':0.33671463,'ProbHCS(i-2)':0.35513745, 'ProbHCS(i-1)':0.35675758,'ProbHCS(i)':0.35708354,'ProbHCS(i+1)':0.35608997,'ProbHCS(i+2)':0.35384281, 'ProbCCS(i-2)':0.30641198,'ProbCCS(i-1)':0.30545186,'ProbCCS(i)':0.30481465,'ProbCCS(i+1)':0.30637661,'ProbCCS(i+2)':0.30944256,'ProbBAA(i-2)': 0.31902793, 'ProbBAA(i-1)': 0.31976632, 'ProbBAA(i)': 0.31988712, 'ProbBAA(i+1)': 0.31979336, 'ProbBAA(i+2)': 0.31959908, 'ProbHAA(i-2)':0.32380660,'ProbHAA(i-1)':0.32390836,'ProbHAA(i)':0.32390634,'ProbHAA(i+1)':0.32390487,'ProbHAA(i+2)':0.32372071, 'ProbCAA(i-2)': 0.35716547,'ProbCAA(i-1)': 0.35632531,'ProbCAA(i)': 0.35620654,'ProbCAA(i+1)': 0.35630177,'ProbCAA(i+2)': 0.35668021, 'RCI(i-2)': 0.02781391, 'RCI(i-1)': 0.02738190, 'RCI(i)': 0.02724255,'RCI(i+1)': 0.02738633,'RCI(i+2)': 0.02778022,'RSA(i-2)':0.36083340, 'RSA(i-1)':0.35928666, 'RSA(i)':0.35889362,'RSA(i+1)':0.35961560,'RSA(i+2)': 0.36040531,'BuriedExposed(i-2)':4.65273507, 'BuriedExposed(i-1)':4.64983325, 'BuriedExposed(i)':4.64901035,'BuriedExposed(i+1)':4.65026636,'BuriedExposed(i+2)':4.65087271,'scon(i-2)':0.56255394,'scon(i-1)':0.56342312, 'scon(i)':0.56383136, 'scon(i+1)':0.56368035, 'scon(i+2)':0.56315084, 'PSIPRED(i-2)':2.23667460, 'PSIPRED(i-1)':2.22836112, 'PSIPRED(i)':2.22385798, 'PSIPRED(i+1)':2.22498376,'PSIPRED(i+2)':2.23347045}

stdev={'ProbBCS(i-2)':0.36710380,'ProbBCS(i-1)':0.36769768,'ProbBCS(i)':0.36826282, 'ProbBCS(i+1)':0.36769110,'ProbBCS(i+2)':0.36612556,'ProbHCS(i-2)':0.41281124, 'ProbHCS(i-1)':0.41338545,'ProbHCS(i)':0.41331838,'ProbHCS(i+1)':0.41248113,'ProbHCS(i+2)':0.41050375, 'ProbCCS(i-2)':0.28775045,'ProbCCS(i-1)':0.28802562,'ProbCCS(i)':0.28793695,'ProbCCS(i+1)':0.28820883,'ProbCCS(i+2)':0.28832984,'ProbBAA(i-2)': 0.11337259, 'ProbBAA(i-1)': 0.11334619, 'ProbBAA(i)': 0.11353772, 'ProbBAA(i+1)': 0.11356962, 'ProbBAA(i+2)': 0.11341782, 'ProbHAA(i-2)':0.09173501,'ProbHAA(i-1)':0.09162931,'ProbHAA(i)':0.09163190,'ProbHAA(i+1)':0.09170977,'ProbHAA(i+2)':0.09177027, 'ProbCAA(i-2)': 0.13297294,'ProbCAA(i-1)': 0.13253590,'ProbCAA(i)': 0.13267841,'ProbCAA(i+1)': 0.13279268,'ProbCAA(i+2)': 0.13292208, 'RCI(i-2)': 0.02084975, 'RCI(i-1)': 0.01918388, 'RCI(i)': 0.01867927,'RCI(i+1)': 0.01915435,'RCI(i+2)': 0.02042823,'RSA(i-2)':0.20780915, 'RSA(i-1)':0.20736890, 'RSA(i)':0.20729659,'RSA(i+1)':0.20735112,'RSA(i+2)': 0.20759875,'BuriedExposed(i-2)':0.47611114, 'BuriedExposed(i-1)':0.47703234, 'BuriedExposed(i)':0.47729004,'BuriedExposed(i+1)':0.47689608,'BuriedExposed(i+2)':0.47670459,'scon(i-2)':0.18876065,'scon(i-1)':0.18818761, 'scon(i)':0.18784945, 'scon(i+1)': 0.18821012, 'scon(i+2)':0.18905959,'PSIPRED(i-2)':0.80942146, 'PSIPRED(i-1)':0.81041821, 'PSIPRED(i)':0.81103325, 'PSIPRED(i+1)':0.81120221,'PSIPRED(i+2)':0.81029808}

exposureDict ={'B':4, 'E':5} 
ssDict = {'E':1, 'H':2, 'C':3 }
bmrbid = argv[1]
psipredflag = argv[2]
sconflag = argv[3]

ssProbFile= bmrbid+'.ss_prob_seccs'
#rciFile = bmrbid+'.str.RCI.txt' if isfile(bmrbid+'.str.RCI.txt') else bmrbid+'.str.corr.RCI.txt'
rciFile ="".join( [f for f in listdir(".") if f.startswith(bmrbid) and f.endswith(".RCI.txt")] )
sconFile = bmrbid+'.scon'
predASAFile = bmrbid+'.asafeat.pred'
psipredSSfile = bmrbid+'.ss'
#hmomentFile = bmrbid +'.hmoment'


resnList = []
AAList = dict()
rciDict=dict()
sconDict = dict()
helixmomentDict = dict()
betamomentDict = dict()
avgSurroundingHydroIndex = dict()
strandCSProbDict=dict()
helixCSProbDict=dict()
coilCSProbDict=dict()
strandSeqProbDict=dict()
helixSeqProbDict=dict()
coilSeqProbDict=dict()
rsaDict = dict()
asastateDict= dict()
PSIPREDDict = dict()

fin1 = open(ssProbFile, "r")


# retrieving seconday structure probability calculated from chemical shift  
index = 1
res_index = dict()
for line in fin1:
    tmp = line.split()
    resn = int(tmp[0])
    res_index[index]= resn
    AA = tmp[1] 
    index+=1
    strandCSProb = float(tmp[2])
    helixCSProb = float(tmp[3])
    coilCSProb = float(tmp[4])
    strandSeqProb = float(tmp[5])
    helixSeqProb = float(tmp[6])
    coilSeqProb = float(tmp[7])
    
    resnList.append(int(resn))
    AAList[resn] = AA
    
    strandCSProbDict[resn]= float(strandCSProb)
    helixCSProbDict[resn]= float(helixCSProb)
    coilCSProbDict[resn]= float(coilCSProb)
    
    
    strandSeqProbDict[resn]= float(strandSeqProb)
    helixSeqProbDict[resn]= float(helixSeqProb)
    coilSeqProbDict[resn]= float(coilSeqProb)

    
if os.path.exists(rciFile):
    
    fin2 = open(rciFile, "r")
    # retrieving backbone RCI value
    for line in fin2:
        (resn, rci, AA) = line.split()
        rciDict[int(resn)] = float(rci)
        
    if sconflag == "sconyes":	
        fin3 = open(sconFile, "r")
        # retrieving per-residue conservation score
        for line in fin3:
            if line.strip() and not line.startswith("#"):
                (resn, AA, score) = line.split()
                sconDict[int(resn)] = float(score)
        fin3.close()
            
    fin4= open(predASAFile, "r")
    for line in fin4:
        if line.strip() and not line.startswith("#"):
            (resn, residue, rsa, asaclass) = line.split()
            rsaDict[int(resn)] = float(rsa)
            asastateDict[int(resn)] = exposureDict[asaclass] 
    '''        
    fin5 = open(psipredSSfile, "r")
    for line in fin5:
        if line.strip():
            line=line.lstrip()
            (resn, residue, ss, dummy, dummy, dummy) = line.split()
            PSIPREDDict[int(resn)] = ssDict[ss]
    '''   
    # ordering the residue number in the rci dictionary    
    rciDictOrdered= OrderedDict(sorted(rciDict.items(), key=operator.itemgetter(0)))    
    
    #if psipredflag=="psipredno": 
    out5res = bmrbid+'.ssfeat5res'
    out3res = bmrbid+'.ssfeat3res'
        
    fout1 = open(out3res, 'w')
    fout2 = open(out5res, 'w')
    if sconflag == "sconyes":
        # 3-residue feature header                      
        fout1.write("Resn,Residue,ProbBCS(i-1),ProbBCS(i),ProbBCS(i+1),ProbHCS(i-1),ProbHCS(i),ProbHCS(i+1),ProbCCS(i-1),ProbCCS(i),ProbCCS(i+1),ProbBAA(i-1),ProbBAA(i),ProbBAA(i+1),ProbHAA(i-1),ProbHAA(i),ProbHAA(i+1),ProbCAA(i-1),ProbCAA(i),ProbCAA(i+1),RCI(i-1),RCI(i),RCI(i+1),RSA(i-1),RSA(i),RSA(i+1),BuriedExposed(i-1), BuriedExposed(i),BuriedExposed(i+1),scon(i-1),scon(i),scon(i+1)\n")
    
         
        # 5-residue feature header    
        fout2.write("Resn,Residue,ProbBCS(i-2),ProbBCS(i-1),ProbBCS(i),ProbBCS(i+1),ProbBCS(i+2),ProbHCS(i-2),ProbHCS(i-1),ProbHCS(i),ProbHCS(i+1),ProbHCS(i+2),ProbCCS(i-2),ProbCCS(i-1),ProbCCS(i),ProbCCS(i+1),ProbCCS(i+2),ProbBAA(i-2),ProbBAA(i-1),ProbBAA(i),ProbBAA(i+1),ProbBAA(i+2),ProbHAA(i-2),ProbHAA(i-1),ProbHAA(i),ProbHAA(i+1),ProbHAA(i+2),ProbCAA(i-2),ProbCAA(i-1),ProbCAA(i),ProbCAA(i+1),ProbCAA(i+2),RCI(i-2),RCI(i-1),RCI(i),RCI(i+1),RCI(i+2), RSA(i-2),RSA(i-1),RSA(i),RSA(i+1),RSA(i+2),BuriedExposed(i-2), BuriedExposed(i-1), BuriedExposed(i),BuriedExposed(i+1),BuriedExposed(i+2), scon(i-2),scon(i-1),scon(i),scon(i+1),scon(i+2)\n")
    elif sconflag == "sconno":
        # 3-residue feature header                      
        fout1.write("Resn,Residue,ProbBCS(i-1),ProbBCS(i),ProbBCS(i+1),ProbHCS(i-1),ProbHCS(i),ProbHCS(i+1),ProbCCS(i-1),ProbCCS(i),ProbCCS(i+1),ProbBAA(i-1),ProbBAA(i),ProbBAA(i+1),ProbHAA(i-1),ProbHAA(i),ProbHAA(i+1),ProbCAA(i-1),ProbCAA(i),ProbCAA(i+1),RCI(i-1),RCI(i),RCI(i+1),RSA(i-1),RSA(i),RSA(i+1),BuriedExposed(i-1), BuriedExposed(i),BuriedExposed(i+1)\n")
    
         
        # 5-residue feature header    
        fout2.write("Resn,Residue,ProbBCS(i-2),ProbBCS(i-1),ProbBCS(i),ProbBCS(i+1),ProbBCS(i+2),ProbHCS(i-2),ProbHCS(i-1),ProbHCS(i),ProbHCS(i+1),ProbHCS(i+2),ProbCCS(i-2),ProbCCS(i-1),ProbCCS(i),ProbCCS(i+1),ProbCCS(i+2),ProbBAA(i-2),ProbBAA(i-1),ProbBAA(i),ProbBAA(i+1),ProbBAA(i+2),ProbHAA(i-2),ProbHAA(i-1),ProbHAA(i),ProbHAA(i+1),ProbHAA(i+2),ProbCAA(i-2),ProbCAA(i-1),ProbCAA(i),ProbCAA(i+1),ProbCAA(i+2),RCI(i-2),RCI(i-1),RCI(i),RCI(i+1),RCI(i+2), RSA(i-2),RSA(i-1),RSA(i),RSA(i+1),RSA(i+2),BuriedExposed(i-2), BuriedExposed(i-1), BuriedExposed(i),BuriedExposed(i+1),BuriedExposed(i+2)\n")	
	
    
    if psipredflag=="psipredyes": 
        fin5 = open(psipredSSfile, "r")
        for line in fin5:
            if line.strip() and not line.startswith("#"):
                line=line.lstrip()
                (resn, serial, residue, ss, dummy, dummy, dummy) = line.split()
                resn = int(resn)
                PSIPREDDict[resn] = ssDict[ss]
                #if resn in res_index:
                 #   PSIPREDDict[res_index[resn]] = ssDict[ss]
        fin5.close()
        out5res = bmrbid+'.ssfeatPSIPRED5res'
        out3res = bmrbid+'.ssfeatPSIPRED3res'
        
        fout3 = open(out3res, 'w')
        fout4 = open(out5res, 'w')
	
        if sconflag == "sconyes":
    
	    # 3-residue feature header                      
            fout3.write("Resn,Residue,ProbBCS(i-1),ProbBCS(i),ProbBCS(i+1),ProbHCS(i-1),ProbHCS(i),ProbHCS(i+1),ProbCCS(i-1),ProbCCS(i),ProbCCS(i+1),ProbBAA(i-1),ProbBAA(i),ProbBAA(i+1),ProbHAA(i-1),ProbHAA(i),ProbHAA(i+1),ProbCAA(i-1),ProbCAA(i),ProbCAA(i+1),RCI(i-1),RCI(i),RCI(i+1),RSA(i-1),RSA(i),RSA(i+1),BuriedExposed(i-1), BuriedExposed(i),BuriedExposed(i+1),scon(i-1),scon(i),scon(i+1),PSIPRED(i-1),PSIPRED(i),PSIPRED(i+1)\n")
        
            # 5-residue feature header    
            fout4.write("Resn,Residue,ProbBCS(i-2),ProbBCS(i-1),ProbBCS(i),ProbBCS(i+1),ProbBCS(i+2),ProbHCS(i-2),ProbHCS(i-1),ProbHCS(i),ProbHCS(i+1),ProbHCS(i+2),ProbCCS(i-2),ProbCCS(i-1),ProbCCS(i),ProbCCS(i+1),ProbCCS(i+2),ProbBAA(i-2),ProbBAA(i-1),ProbBAA(i),ProbBAA(i+1),ProbBAA(i+2),ProbHAA(i-2),ProbHAA(i-1),ProbHAA(i),ProbHAA(i+1),ProbHAA(i+2),ProbCAA(i-2),ProbCAA(i-1),ProbCAA(i),ProbCAA(i+1),ProbCAA(i+2),RCI(i-2),RCI(i-1),RCI(i),RCI(i+1),RCI(i+2), RSA(i-2),RSA(i-1),RSA(i),RSA(i+1),RSA(i+2),BuriedExposed(i-2), BuriedExposed(i-1), BuriedExposed(i),BuriedExposed(i+1),BuriedExposed(i+2), scon(i-2),scon(i-1),scon(i),scon(i+1),scon(i+2),PSIPRED(i-2),PSIPRED(i-1),PSIPRED(i),PSIPRED(i+1),PSIPRED(i+2)\n")
        elif sconflag == "sconno":
    
	    # 3-residue feature header                      
            fout3.write("Resn,Residue,ProbBCS(i-1),ProbBCS(i),ProbBCS(i+1),ProbHCS(i-1),ProbHCS(i),ProbHCS(i+1),ProbCCS(i-1),ProbCCS(i),ProbCCS(i+1),ProbBAA(i-1),ProbBAA(i),ProbBAA(i+1),ProbHAA(i-1),ProbHAA(i),ProbHAA(i+1),ProbCAA(i-1),ProbCAA(i),ProbCAA(i+1),RCI(i-1),RCI(i),RCI(i+1),RSA(i-1),RSA(i),RSA(i+1),BuriedExposed(i-1), BuriedExposed(i),BuriedExposed(i+1), PSIPRED(i-1),PSIPRED(i),PSIPRED(i+1)\n")
        
            # 5-residue feature header    
            fout4.write("Resn,Residue,ProbBCS(i-2),ProbBCS(i-1),ProbBCS(i),ProbBCS(i+1),ProbBCS(i+2),ProbHCS(i-2),ProbHCS(i-1),ProbHCS(i),ProbHCS(i+1),ProbHCS(i+2),ProbCCS(i-2),ProbCCS(i-1),ProbCCS(i),ProbCCS(i+1),ProbCCS(i+2),ProbBAA(i-2),ProbBAA(i-1),ProbBAA(i),ProbBAA(i+1),ProbBAA(i+2),ProbHAA(i-2),ProbHAA(i-1),ProbHAA(i),ProbHAA(i+1),ProbHAA(i+2),ProbCAA(i-2),ProbCAA(i-1),ProbCAA(i),ProbCAA(i+1),ProbCAA(i+2),RCI(i-2),RCI(i-1),RCI(i),RCI(i+1),RCI(i+2), RSA(i-2),RSA(i-1),RSA(i),RSA(i+1),RSA(i+2),BuriedExposed(i-2), BuriedExposed(i-1), BuriedExposed(i),BuriedExposed(i+1),BuriedExposed(i+2),PSIPRED(i-2),PSIPRED(i-1),PSIPRED(i),PSIPRED(i+1),PSIPRED(i+2)\n")        

    for key in rciDictOrdered.keys():
        currentRes = key
        previousRes = key - 1
        prevprevRes = key - 2
        nextRes = key + 1
        nextnextRes = key + 2
        
             
        # 3-residue feature set    
        if (previousRes in rciDictOrdered and currentRes in rciDictOrdered and nextRes in rciDictOrdered) and (previousRes in rsaDict and currentRes in rsaDict and nextRes in rsaDict):
            # feature z-score normalization
            strandCSProbPrev = (strandCSProbDict[previousRes] - mean['ProbBCS(i-1)']) / stdev['ProbBCS(i-1)']
            strandCSProbCurrent = (strandCSProbDict[currentRes] - mean['ProbBCS(i)']) / stdev['ProbBCS(i)']
            strandCSProbNext = (strandCSProbDict[nextRes] - mean['ProbBCS(i+1)']) / stdev['ProbBCS(i+1)']
            
            helixCSProbPrev = (helixCSProbDict[previousRes] - mean['ProbHCS(i-1)']) / stdev['ProbHCS(i-1)']
            helixCSProbCurrent = (helixCSProbDict[currentRes] - mean['ProbHCS(i)']) / stdev['ProbHCS(i)']
            helixCSProbNext = (helixCSProbDict[nextRes] - mean['ProbHCS(i+1)']) / stdev['ProbHCS(i+1)']
    
            coilCSProbPrev= (coilCSProbDict[previousRes] - mean['ProbCCS(i-1)']) / stdev['ProbCCS(i-1)']
            coilCSProbCurrent= (coilCSProbDict[currentRes] - mean['ProbCCS(i)'])/ stdev['ProbCCS(i)']
            coilCSProbNext= (coilCSProbDict[nextRes]- mean['ProbCCS(i+1)'])/ stdev['ProbCCS(i+1)']

            strandSeqProbPrev = (strandSeqProbDict[previousRes] - mean['ProbBAA(i-1)']) / stdev['ProbBAA(i-1)']
            strandSeqProbCurrent = (strandSeqProbDict[currentRes] - mean['ProbBAA(i)']) / stdev['ProbBAA(i)']
            strandSeqProbNext = (strandSeqProbDict[nextRes] - mean['ProbBAA(i+1)']) / stdev['ProbBAA(i+1)']
            
            helixSeqProbPrev = (helixSeqProbDict[previousRes] - mean['ProbHAA(i-1)']) / stdev['ProbHAA(i-1)']
            helixSeqProbCurrent = (helixSeqProbDict[currentRes] - mean['ProbHAA(i)']) / stdev['ProbHAA(i)']
            helixSeqProbNext = (helixSeqProbDict[nextRes] - mean['ProbHAA(i+1)']) / stdev['ProbHAA(i+1)']
            
            coilSeqProbPrev= (coilSeqProbDict[previousRes] - mean['ProbCAA(i-1)']) / stdev['ProbCAA(i-1)']
            coilSeqProbCurrent= (coilSeqProbDict[currentRes] - mean['ProbCAA(i)'])/ stdev['ProbCAA(i)']
            coilSeqProbNext= (coilSeqProbDict[nextRes]- mean['ProbCAA(i+1)'])/ stdev['ProbCAA(i+1)']
            
            rciPrev= (rciDict[previousRes]- mean['RCI(i-1)']) / stdev['RCI(i-1)']
            rciCurrent= (rciDict[currentRes]- mean['RCI(i)']) / stdev['RCI(i)']
            rciNext= (rciDict[nextRes]- mean['RCI(i+1)']) / stdev['RCI(i+1)']
            
            rsaPrev= (rsaDict[previousRes]- mean['RSA(i-1)']) / stdev['RSA(i-1)']
            rsaCurrent= (rsaDict[currentRes]- mean['RSA(i)']) / stdev['RSA(i)']
            rsaNext= (rsaDict[nextRes]- mean['RSA(i+1)']) / stdev['RSA(i+1)']
            
            asastatePrev= (asastateDict[previousRes]- mean['BuriedExposed(i-1)']) / stdev['BuriedExposed(i-1)']
            asastateCurrent= (asastateDict[currentRes]- mean['BuriedExposed(i)']) / stdev['BuriedExposed(i)']
            asastateNext= (asastateDict[nextRes]- mean['BuriedExposed(i+1)']) / stdev['BuriedExposed(i+1)']
            if sconflag == "sconyes":
                sconPrev= (sconDict[previousRes]- mean['scon(i-1)']) / stdev['scon(i-1)']
                sconCurrent= (sconDict[currentRes]- mean['scon(i)']) / stdev['scon(i)']
                sconNext= (sconDict[nextRes]- mean['scon(i+1)']) / stdev['scon(i+1)']
                featureStr = "%d,%s,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f ,%f,%f" %(currentRes, three2one[AAList[currentRes]], strandCSProbPrev, strandCSProbCurrent, strandCSProbNext, helixCSProbPrev, helixCSProbCurrent, helixCSProbNext, coilCSProbPrev, coilCSProbCurrent, coilCSProbNext,strandSeqProbPrev, strandSeqProbCurrent, strandSeqProbNext, helixSeqProbPrev, helixSeqProbCurrent, helixSeqProbNext, coilSeqProbPrev, coilSeqProbCurrent, coilSeqProbNext, rciPrev, rciCurrent,rciNext, rsaPrev, rsaCurrent, rsaNext, asastatePrev, asastateCurrent, asastateNext, sconPrev, sconCurrent, sconNext)
            elif sconflag == "sconno":
                featureStr = "%d,%s,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f" %(currentRes, three2one[AAList[currentRes]], strandCSProbPrev, strandCSProbCurrent, strandCSProbNext, helixCSProbPrev, helixCSProbCurrent, helixCSProbNext, coilCSProbPrev, coilCSProbCurrent, coilCSProbNext,strandSeqProbPrev, strandSeqProbCurrent, strandSeqProbNext, helixSeqProbPrev, helixSeqProbCurrent, helixSeqProbNext, coilSeqProbPrev, coilSeqProbCurrent, coilSeqProbNext, rciPrev, rciCurrent,rciNext, rsaPrev, rsaCurrent, rsaNext, asastatePrev, asastateCurrent, asastateNext)     
                
            fout1.write("%s\n" %(featureStr))
            if psipredflag == "psipredyes":
                PSIPREDPrev= (PSIPREDDict[previousRes]- mean['PSIPRED(i-1)']) / stdev['PSIPRED(i-1)']
                PSIPREDCurrent= (PSIPREDDict[currentRes]- mean['PSIPRED(i)']) / stdev['PSIPRED(i)']
                PSIPREDNext= (PSIPREDDict[nextRes]- mean['PSIPRED(i+1)']) / stdev['PSIPRED(i+1)'] 
                #fout3.write("%d,%s,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f ,%f,%f,%f,%f,%f\n" %(currentRes, three2one[AAList[currentRes]], strandCSProbPrev, strandCSProbCurrent, strandCSProbNext, helixCSProbPrev, helixCSProbCurrent, helixCSProbNext, coilCSProbPrev, coilCSProbCurrent, coilCSProbNext,strandSeqProbPrev, strandSeqProbCurrent, strandSeqProbNext, helixSeqProbPrev, helixSeqProbCurrent, helixSeqProbNext, coilSeqProbPrev, coilSeqProbCurrent, coilSeqProbNext, rciPrev, rciCurrent,rciNext, rsaPrev, rsaCurrent, rsaNext, asastatePrev, asastateCurrent, asastateNext, sconPrev, sconCurrent, sconNext, PSIPREDPrev, PSIPREDCurrent, PSIPREDNext))
                fout3.write("%s,%f,%f,%f\n"%(featureStr,PSIPREDPrev, PSIPREDCurrent, PSIPREDNext)) 
                
        # 5-residue feature set    
        if (prevprevRes in rciDict and previousRes in rciDictOrdered and currentRes in rciDictOrdered and nextRes in rciDictOrdered and nextnextRes in rciDictOrdered) and (prevprevRes in rsaDict and previousRes in rsaDict and currentRes in rsaDict and nextRes in rsaDict and nextnextRes in rsaDict):
                
                strandCSProbPrev = (strandCSProbDict[previousRes] - mean['ProbBCS(i-1)']) / stdev['ProbBCS(i-1)']
                strandCSProbCurrent = (strandCSProbDict[currentRes] - mean['ProbBCS(i)']) / stdev['ProbBCS(i)']
                strandCSProbNext = (strandCSProbDict[nextRes] - mean['ProbBCS(i+1)']) / stdev['ProbBCS(i+1)']
                
                helixCSProbPrev = (helixCSProbDict[previousRes] - mean['ProbHCS(i-1)']) / stdev['ProbHCS(i-1)']
                helixCSProbCurrent = (helixCSProbDict[currentRes] - mean['ProbHCS(i)']) / stdev['ProbHCS(i)']
                helixCSProbNext = (helixCSProbDict[nextRes] - mean['ProbHCS(i+1)']) / stdev['ProbHCS(i+1)']
        
                coilCSProbPrev= (coilCSProbDict[previousRes] - mean['ProbCCS(i-1)']) / stdev['ProbCCS(i-1)']
                coilCSProbCurrent= (coilCSProbDict[currentRes] - mean['ProbCCS(i)'])/ stdev['ProbCCS(i)']
                coilCSProbNext= (coilCSProbDict[nextRes]- mean['ProbCCS(i+1)'])/ stdev['ProbCCS(i+1)']

                strandSeqProbPrev = (strandSeqProbDict[previousRes] - mean['ProbBAA(i-1)']) / stdev['ProbBAA(i-1)']
                strandSeqProbCurrent = (strandSeqProbDict[currentRes] - mean['ProbBAA(i)']) / stdev['ProbBAA(i)']
                strandSeqProbNext = (strandSeqProbDict[nextRes] - mean['ProbBAA(i+1)']) / stdev['ProbBAA(i+1)']
                
                helixSeqProbPrev = (helixSeqProbDict[previousRes] - mean['ProbHAA(i-1)']) / stdev['ProbHAA(i-1)']
                helixSeqProbCurrent = (helixSeqProbDict[currentRes] - mean['ProbHAA(i)']) / stdev['ProbHAA(i)']
                helixSeqProbNext = (helixSeqProbDict[nextRes] - mean['ProbHAA(i+1)']) / stdev['ProbHAA(i+1)']
                
                coilSeqProbPrev= (coilSeqProbDict[previousRes] - mean['ProbCAA(i-1)']) / stdev['ProbCAA(i-1)']
                coilSeqProbCurrent= (coilSeqProbDict[currentRes] - mean['ProbCAA(i)'])/ stdev['ProbCAA(i)']
                coilSeqProbNext= (coilSeqProbDict[nextRes]- mean['ProbCAA(i+1)'])/ stdev['ProbCAA(i+1)']
                
                rciPrev= (rciDict[previousRes]- mean['RCI(i-1)']) / stdev['RCI(i-1)']
                rciCurrent= (rciDict[currentRes]- mean['RCI(i)']) / stdev['RCI(i)']
                rciNext= (rciDict[nextRes]- mean['RCI(i+1)']) / stdev['RCI(i+1)']
                
                rsaPrev= (rsaDict[previousRes]- mean['RSA(i-1)']) / stdev['RSA(i-1)']
                rsaCurrent= (rsaDict[currentRes]- mean['RSA(i)']) / stdev['RSA(i)']
                rsaNext= (rsaDict[nextRes]- mean['RSA(i+1)']) / stdev['RSA(i+1)']
                
                asastatePrev= (asastateDict[previousRes]- mean['BuriedExposed(i-1)']) / stdev['BuriedExposed(i-1)']
                asastateCurrent= (asastateDict[currentRes]- mean['BuriedExposed(i)']) / stdev['BuriedExposed(i)']
                asastateNext= (asastateDict[nextRes]- mean['BuriedExposed(i+1)']) / stdev['BuriedExposed(i+1)']
                
                
                
            
                #strand probability from chem shift
                strandCSProbPrevPrev = (strandCSProbDict[prevprevRes] - mean['ProbBCS(i-2)']) / stdev['ProbBCS(i-2)']
                strandCSProbNextNext = (strandCSProbDict[nextnextRes] - mean['ProbBCS(i+2)']) / stdev['ProbBCS(i+2)']   
                
                #helix probability from chem shift
                helixCSProbPrevPrev = (helixCSProbDict[prevprevRes] - mean['ProbHCS(i-2)']) / stdev['ProbHCS(i-2)']
                helixCSProbNextNext = (helixCSProbDict[nextnextRes] - mean['ProbHCS(i+2)']) / stdev['ProbHCS(i+2)']
                
                #coil probability from chem shift
                coilCSProbPrevPrev= (coilCSProbDict[prevprevRes] - mean['ProbCCS(i-2)']) / stdev['ProbCCS(i-2)']
                coilCSProbNextNext= (coilCSProbDict[nextnextRes]- mean['ProbCCS(i+2)'])/ stdev['ProbCCS(i+2)']
        
                #strand probability from seq
                strandSeqProbPrevPrev = (strandSeqProbDict[prevprevRes] - mean['ProbBAA(i-2)']) / stdev['ProbBAA(i-2)']
                strandSeqProbNextNext = (strandSeqProbDict[nextnextRes] - mean['ProbBAA(i+2)']) / stdev['ProbBAA(i+2)']
                
                 #helix probability from seq
                helixSeqProbPrevPrev = (helixSeqProbDict[prevprevRes] - mean['ProbHAA(i-2)']) / stdev['ProbHAA(i-2)']
                helixSeqProbNextNext = (helixSeqProbDict[nextnextRes] - mean['ProbHAA(i+2)']) / stdev['ProbHAA(i+2)']
                
                #coil probability from seq
                coilSeqProbPrevPrev= (coilSeqProbDict[prevprevRes] - mean['ProbCAA(i-2)']) / stdev['ProbCAA(i-2)']
                coilSeqProbNextNext= (coilSeqProbDict[nextnextRes]- mean['ProbCAA(i+2)'])/ stdev['ProbCAA(i+2)']
                
                #rci
                rciPrevPrev= (rciDict[prevprevRes]- mean['RCI(i-2)']) / stdev['RCI(i-2)']
                rciNextNext= (rciDict[nextnextRes]- mean['RCI(i+2)']) / stdev['RCI(i+2)']
        
                #RSA 
                rsaPrevPrev= (rsaDict[prevprevRes]- mean['RSA(i-2)']) / stdev['RSA(i-2)']
                rsaNextNext= (rsaDict[nextnextRes]- mean['RSA(i+2)']) / stdev['RSA(i+2)']
                
                #buried/exposed binary class: (4/5)
                asastatePrevPrev= (asastateDict[prevprevRes]- mean['BuriedExposed(i-2)']) / stdev['BuriedExposed(i-2)']
                asastateNextNext= (asastateDict[nextnextRes]- mean['BuriedExposed(i+2)']) / stdev['BuriedExposed(i+2)']
                
                if sconflag == "sconyes":
		    # conservation score
                    sconPrev= (sconDict[previousRes]- mean['scon(i-1)']) / stdev['scon(i-1)']
                    sconCurrent= (sconDict[currentRes]- mean['scon(i)']) / stdev['scon(i)']
                    sconNext= (sconDict[nextRes]- mean['scon(i+1)']) / stdev['scon(i+1)']
                    sconPrevPrev= (sconDict[prevprevRes]- mean['scon(i-2)']) / stdev['scon(i-2)']
                    sconNextNext= (sconDict[nextnextRes]- mean['scon(i+2)']) / stdev['scon(i+2)']
                    featureStr = "%d,%s,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f ,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f" %(currentRes, three2one[AAList[currentRes]], strandCSProbPrevPrev, strandCSProbPrev, strandCSProbCurrent, strandCSProbNext, strandCSProbNextNext, helixCSProbPrevPrev, helixCSProbPrev, helixCSProbCurrent, helixCSProbNext, helixCSProbNextNext, coilCSProbPrevPrev, coilCSProbPrev, coilCSProbCurrent, coilCSProbNext,coilCSProbNextNext, strandSeqProbPrevPrev, strandSeqProbPrev, strandSeqProbCurrent, strandSeqProbNext, strandSeqProbNextNext, helixSeqProbPrevPrev, helixSeqProbPrev, helixSeqProbCurrent, helixSeqProbNext, helixSeqProbNextNext, coilSeqProbPrevPrev, coilSeqProbPrev, coilSeqProbCurrent, coilSeqProbNext,coilSeqProbNextNext, rciPrevPrev, rciPrev, rciCurrent,rciNext, rciNextNext, rsaPrevPrev, rsaPrev, rsaCurrent,rsaNext, rsaNextNext, asastatePrevPrev, asastatePrev, asastateCurrent, asastateNext, asastateNextNext, sconPrevPrev, sconPrev, sconCurrent, sconNext, sconNextNext)


                elif sconflag == "sconno":
                    featureStr = "%d,%s,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f ,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f" %(currentRes, three2one[AAList[currentRes]], strandCSProbPrevPrev, strandCSProbPrev, strandCSProbCurrent, strandCSProbNext, strandCSProbNextNext, helixCSProbPrevPrev, helixCSProbPrev, helixCSProbCurrent, helixCSProbNext, helixCSProbNextNext, coilCSProbPrevPrev, coilCSProbPrev, coilCSProbCurrent, coilCSProbNext,coilCSProbNextNext, strandSeqProbPrevPrev, strandSeqProbPrev, strandSeqProbCurrent, strandSeqProbNext, strandSeqProbNextNext, helixSeqProbPrevPrev, helixSeqProbPrev, helixSeqProbCurrent, helixSeqProbNext, helixSeqProbNextNext, coilSeqProbPrevPrev, coilSeqProbPrev, coilSeqProbCurrent, coilSeqProbNext,coilSeqProbNextNext, rciPrevPrev, rciPrev, rciCurrent,rciNext, rciNextNext, rsaPrevPrev, rsaPrev, rsaCurrent,rsaNext, rsaNextNext, asastatePrevPrev, asastatePrev, asastateCurrent, asastateNext, asastateNextNext)	
                   
                
                fout2.write("%s\n" %featureStr)
                
           
                if psipredflag == "psipredyes":
		    #PSIPRED- predicted SS
                    PSIPREDPrevPrev= (PSIPREDDict[prevprevRes]- mean['PSIPRED(i-2)']) / stdev['PSIPRED(i-2)']
                    PSIPREDPrev= (PSIPREDDict[previousRes]- mean['PSIPRED(i-1)']) / stdev['PSIPRED(i-1)']
                    PSIPREDCurrent= (PSIPREDDict[currentRes]- mean['PSIPRED(i)']) / stdev['PSIPRED(i)']
                    PSIPREDNext= (PSIPREDDict[nextRes]- mean['PSIPRED(i+1)']) / stdev['PSIPRED(i+1)'] 
                    PSIPREDNextNext= (PSIPREDDict[nextnextRes]- mean['PSIPRED(i+2)']) / stdev['PSIPRED(i+2)']        
                    
                    #fout4.write("%d,%s,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f ,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f\n" %(currentRes, three2one[AAList[currentRes]], strandCSProbPrevPrev, strandCSProbPrev, strandCSProbCurrent, strandCSProbNext, strandCSProbNextNext, helixCSProbPrevPrev, helixCSProbPrev, helixCSProbCurrent, helixCSProbNext, helixCSProbNextNext, coilCSProbPrevPrev, coilCSProbPrev, coilCSProbCurrent, coilCSProbNext,coilCSProbNextNext, strandSeqProbPrevPrev, strandSeqProbPrev, strandSeqProbCurrent, strandSeqProbNext, strandSeqProbNextNext, helixSeqProbPrevPrev, helixSeqProbPrev, helixSeqProbCurrent, helixSeqProbNext, helixSeqProbNextNext, coilSeqProbPrevPrev, coilSeqProbPrev, coilSeqProbCurrent, coilSeqProbNext,coilSeqProbNextNext, rciPrevPrev, rciPrev, rciCurrent,rciNext, rciNextNext, rsaPrevPrev, rsaPrev, rsaCurrent,rsaNext, rsaNextNext, asastatePrevPrev, asastatePrev, asastateCurrent, asastateNext, asastateNextNext, sconPrevPrev, sconPrev, sconCurrent, sconNext, sconNextNext,PSIPREDPrevPrev, PSIPREDPrev, PSIPREDCurrent, PSIPREDNext, PSIPREDNextNext))     
                    fout4.write("%s,%f,%f,%f,%f,%f\n"%(featureStr, PSIPREDPrevPrev, PSIPREDPrev, PSIPREDCurrent, PSIPREDNext, PSIPREDNextNext))
            
    
fin1.close()
fin2.close()
fin4.close()
fout1.close()
fout2.close()

