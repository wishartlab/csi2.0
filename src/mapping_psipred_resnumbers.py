from os.path import isfile, join
import operator
import sys

three2one ={'ALA' : 'A','ARG' : 'R','ASN' : 'N','ASP' : 'D','ASX' : 'B','CYS' : 'C','GLN' : 'Q',
             'GLU' : 'E','GLX' : 'Z','GLY' : 'G','HIS' : 'H','ILE' : 'I','LEU' :'L','LYS' : 'K',
             'MET' : 'M','PHE' : 'F','PRO' : 'P','SER' : 'S','THR' : 'T','TRP' :'W','TYR' : 'Y',
             'VAL' : 'V','PCA' : 'X'}


bmrbfile= sys.argv[1]
tmp = bmrbfile.split('.')
psipredfile = tmp[0]+'.ss2'
outfile = tmp[0]+ '.ss'
fin1 = open (bmrbfile, 'r')
fin2 = open (psipredfile, 'r')
fout = open (outfile, 'w')

resnList=[]
for line in fin1:
    #if line.strip() and line[0].isdigit() and len(line.split()) == 5:
    if line.startswith("SEQ"):
        elems = line.split()
        for elem in elems:
            if elem.isalnum() and (elem != "0" and elem!="SEQ"):
                resnList.append(elem[:-3])
                
psipredLines=[]                
for line in fin2:
    line= line.strip()
    if line.strip() and not line.startswith("#") and line[0].isdigit():
        psipredLines.append(line)
        
fout.write ("#bmrbResNum Serial AA SS ProbCoil ProbHelix ProbBeta\n\n")        
for i in range (len(psipredLines)):
    fout.write("%s %s\n" %(resnList[i], psipredLines[i]))
