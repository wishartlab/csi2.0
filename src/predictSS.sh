#!/bin/bash
# a stand-alone program to predict fractional ASA from experimental shift
# it takes input bmrbid as only input 
# steps to predict fractional ASA from bmrb file
# a. bmrb2talos conversion - a shell script
# b. seccs with nrc calculation - perl script
# c. sec str. probability with incomplete shift - perl script
# d. backbone RCI prediction - a system call
# e. calculate per-residue conservation score
# f. create 3 and 5-residue window feature file - python script
# g. invoke multiclass SVM model to predict on unknown protein - an R script


export R_LIBS=/usr/lib64/R/library
bmrbid=$1
psipred_flag=$2
scon_flag=$3
R_DEFAULT_PACKAGES='utils,grDevices,graphics,stats'
######################################
# feature file creation and prediction
######################################
python3 /apps/csi/project/Standalone_CSI2.0/src/createSSPredFeatureFile.py $bmrbid $psipred_flag $scon_flag
/usr/local/bin/Rscript --vanilla /apps/csi/project/Standalone_CSI2.0/src/predictSS.r $bmrbid $psipred_flag $scon_flag
