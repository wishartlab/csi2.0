#!/usr/local/bin/Rscript 

library(caret)
library(kernlab)
library(hash)

Args <- commandArgs(TRUE)

if (length(Args) < 3) {
  cat("\nError: No input file supplied and psipred/ scon options are provided!")
  cat("\n\t Syntax: predictSS.R <input-bmrb> psipred<yes,no> scon<yes,no>\n\n")
  quit()
}


## Input bmrb feature file

psipredflag = Args[2]
sconflag = Args[3]

#read the asa feature file and extract the residue list and resn list from there
#asa.data = read.table(file=paste(Args[1], "asafeat.pred", sep="."), sep=' ', comment.char="#", blank.lines.skip=TRUE)
#resnList = asa.data[,1]
#residueList = asa.data[,2]
#len = length(residueList)
# the above code has been abrogated
###############################################################################
# reading the psipred predicted secondary structure prediction
if (psipredflag== "psipredyes"){
  psipred.data = read.table (file=paste(Args[1],"ss", sep ="."), sep=" ", header = FALSE, blank.lines.skip=TRUE, comment.char ="#")
  psipred.ss = psipred.data[,4] 

  psipred.resn = psipred.data[,1]
  psipred.residue = psipred.data[,3]
  psipred.hash = hash(keys= psipred.resn, values = psipred.ss)
}   
###############################################################################
# This part will execute nevertheless the psipredflag value
if (sconflag  == "sconyes"){
  infile1 = paste(Args[1], "ssfeat3res", sep=".")
  infile2 = paste(Args[1], "ssfeat5res", sep=".")
  load("/apps/csi/project/Standalone_CSI2.0/models/SVMRadialParamTunedModelScaled_incomplete_shift_refdssp_3res_scon.RData")
  load("/apps/csi/project/Standalone_CSI2.0/models/SVMRadialParamTunedModelScaled_incomplete_shift_refdssp_sconFeat.RData")
  test.3resfeatures = read.table(file=infile1, sep=',', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
  x=test.3resfeatures[,3:32]
  resnList1 = test.3resfeatures[,1]
  residueList1 = test.3resfeatures[,2]
  testData = data.frame(x)
  pred3resnoPsipred=predict(SVMRadialFitScaled3resScon$finalModel, testData)
     
  
  
  test.5resfeatures = read.table(file=infile2, sep=',', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
  x=test.5resfeatures[,3:52]
  resnList2 = test.5resfeatures[,1]
  residueList2 = test.5resfeatures[,2]
  testData = data.frame(x)
  pred5resnoPsipred=predict(SVMRadialFitScaledSconFeat$finalModel, testData)
  
  
  residue = hash()
  residue [[as.character(resnList1[1])]] = paste(residueList1[1])
  for (i in 1:length(resnList2)){ 
    residue[[as.character(resnList2[i])]]= paste(residueList2[i])
  }
  residue [[as.character(resnList1[length(resnList1)])]] = paste(residueList1[length(residueList1)])
  
  finalSSvector = hash()
  #****
  # we can't do the first and last residue assignment as the residue name is unknown in these two locations
  # from the feature file (feature files are 3 and 5-residue based)
  #***
  #finalSSvector[[as.character(resnList1[1]-1)]]='C'
  finalSSvector[[as.character(resnList1[1])]] = paste(pred3resnoPsipred[1])
  
  
  for (i in 1:length(resnList2)){ 
        finalSSvector[[as.character(resnList2[i])]]= paste(pred5resnoPsipred[i])
  }


  finalSSvector[[as.character(resnList1[length(resnList1)])]] = paste(pred3resnoPsipred[length(pred3resnoPsipred)])
  #finalSSvector[[as.character(resnList1[length(resnList1)+1])]] = 'C'
  
  
}

  
#else 
if (sconflag == "sconyes" && psipredflag == "psipredyes"){
  infile1 = paste(Args[1], "ssfeatPSIPRED3res", sep=".")
  infile2 = paste(Args[1], "ssfeatPSIPRED5res", sep=".")
  load("/apps/csi/project/Standalone_CSI2.0/models/SVMRadialParamTunedModelScaled_incomplete_shift_refdssp_3res_scon_psipred.RData")
  load("/apps/csi/project/Standalone_CSI2.0/models/SVMRadialParamTunedModelScaled_incomplete_shift_refdssp_scon_psipred.RData")
  test.3resfeaturesPsipred = read.table(file=infile1, sep=',', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
  resnList1 = test.3resfeaturesPsipred[,1]
  testData = data.frame(x=test.3resfeaturesPsipred[,3:35])
  pred3resPsipred=predict(SVMRadialFitScaled3resSconPsipred$finalModel, testData)
  
  test.5resfeaturesPsipred = read.table(file=infile2, sep=',', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
  resnList2 = test.5resfeaturesPsipred[,1]
  testData = data.frame(x=test.5resfeaturesPsipred[,3:57])
  pred5resPsipred=predict(SVMRadialFitScaledSconPsipred$finalModel, testData)
  
}else if (sconflag == "sconno" && psipredflag == "psipredyes"){
  infile1 = paste(Args[1], "ssfeatPSIPRED3res", sep=".")
  infile2 = paste(Args[1], "ssfeatPSIPRED5res", sep=".")
  load("/apps/csi/project/Standalone_CSI2.0/models/SVMRadialParamTunedModelScaled_incomplete_shift_refdssp_3res_psipred.RData")
  load("/apps/csi/project/Standalone_CSI2.0/models/SVMRadialParamTunedModelScaled_incomplete_shift_refdssp_psipred.RData")
  
  test.3resfeaturesPsipred = read.table(file=infile1, sep=',', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
  resnList1 = test.3resfeaturesPsipred[,1]
  testData = data.frame(x=test.3resfeaturesPsipred[,3:32])
  pred3resPsipred=predict(SVMRadialFitScaled3resPsipred$finalModel, testData)
  
  
  test.5resfeaturesPsipred = read.table(file=infile2, sep=',', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
  resnList2 = test.5resfeaturesPsipred[,1]
  testData = data.frame(x=test.5resfeaturesPsipred[,3:52])
  pred5resPsipred=predict(SVMRadialFitScaledPsipred$finalModel, testData)
}



    

if (psipredflag =="psipredyes" && sconflag == "sconyes"){
  
  finalSSvectorPsipred = hash()
  
  #finalSSvector[1]='C'
  finalSSvectorPsipred[[as.character(resnList1[1])]] = paste(pred3resPsipred[1])
  
  #print(length(pred5res))
  for (i in 1:length(resnList2)){ 
    finalSSvectorPsipred[[as.character(resnList2[i])]]= paste(pred5resPsipred[i])
  }
  
  
  finalSSvectorPsipred[[as.character(resnList1[length(resnList1)])]] = paste(pred3resPsipred[length(pred3resPsipred)])
    #finalSSvectorPsipred[end_index+2] = 'C'

    # compare the agreement between the predicted ss with psipred and w/o psipred
    # if agreement < 95%, report the prdiction without psipred (prediction from experimental information)
    agreement = 0
    k = keys(finalSSvector)
    for (m in 1:length(k)){
        key = as.character(k[m])
	      if (finalSSvector[[key]] == finalSSvectorPsipred[[key]]){
	
	          agreement= agreement + 1	
	      }
	    
    }
    
    agreement = agreement/length(finalSSvector)*100
    
    if (agreement > 95){
	      finalSSvector =  finalSSvectorPsipred    
    }else {
	      finalSSvector = finalSSvector
    }
  
}else if (psipredflag =="psipredyes" && sconflag == "sconno"){
  finalSSvectorPsipred = hash()
  
  cat(pred5resPsipred)
  finalSSvectorPsipred[[as.character(resnList1[1])]] = paste(pred3resPsipred[1])
  
  
  for (i in 1:length(resnList2)){ 
    finalSSvectorPsipred[[as.character(resnList2[i])]]= paste(pred5resPsipred[i])
  }
  
  
  finalSSvectorPsipred[[as.character(resnList1[length(resnList1)])]] = paste(pred3resPsipred[length(pred3resPsipred)])
    
  finalSSvector =  finalSSvectorPsipred
}

#############################################################################

if (psipredflag == "psipredyes"){
  for (i in 1:length(psipred.resn)){
    key = as.character(psipred.resn[i])
    #cat("\n",key,psipred.hash[[key]], sep=" ")
    if (all(has.key(key, finalSSvector)) == FALSE){
      finalSSvector[[key]] = psipred.hash[[key]]
    }
  } 
}  
#############################################################################

#cat(finalSSvector)
#print(end_index+2)
outfile1 = paste(Args[1], "sspred1", sep=".")
outfile2 = paste(Args[1], "sspred2", sep=".")

#==========================================================
#vertical display of secondary structure prediction result
#==========================================================

con=file(outfile1, "w")
oheader = "#Resn Residue PredictedSS";
cat(file=con, append=TRUE, oheader, "\n")

###############################################################################
if(psipredflag == "psipredyes"){
  for (i in 1:length(psipred.resn)){
    
    key = as.character(psipred.resn[i])
    if (finalSSvector[[key]] == "E"){
      finalSSvector[[key]] = "B"
    }
    ostr = paste (psipred.resn[i], psipred.residue[i], finalSSvector[[key]], sep=" ")
    cat( file=con, append=TRUE, ostr, "\n" )
  }
}else if(psipredflag == "psipredno"){
  k = as.numeric(keys(finalSSvector))
  k = sort(k , decreasing=FALSE)
  
  for (i in 1:length(k)){
       key = as.character(k[i])
       ostr = paste (k[i], residue[[key]], finalSSvector[[key]], sep=" ")
       cat( file=con, append=TRUE, ostr, "\n" )
    }
}
close(con)
###############################################################################

#==========================================================
#horizontal display of secondary structure prediction result
#==========================================================
finalSS.data = read.table(file=paste(Args[1],"sspred1", sep ="."), sep=" ", header = FALSE, blank.lines.skip=TRUE, comment.char ="#")
resnList= finalSS.data[,1]
residueList= finalSS.data[,2] 
finalSSvector= finalSS.data[,3]


con=file(outfile2, "w")
oheader = "# CSI2.0 predicted SS";
#cat(file=con, append=TRUE, oheader, "\n")

len = length(finalSSvector)
n = ceiling(len/50)
j = 1
AAvector = NULL

for (i in 1:n){
    start = j
    end = j+50-1  

    if(end > len){
	      end = len
    }	    
    SSvector = finalSSvector[start:end]
    SSvector=paste(SSvector, sep='', collapse='')	
    AAvector = residueList[start:end]
    AAvector=paste(AAvector, sep='', collapse='') 	
    SSstr = paste (resnList[start], SSvector, resnList[end], sep="   ")
    AAstr = paste(resnList[start], AAvector, resnList[end], sep ="   ")
 
    #save to a file
    cat( file=con, append=TRUE, AAstr, "\n" ) 	 
    cat( file=con, append=TRUE, SSstr, "\n\n" )
    
    j = j+ 50
}

close(con)
