#!/usr/local/bin/python3

import sys

resmap = {'A' : 'ALA','R' : 'ARG','N' : 'ASN','D' : 'ASP','B' : 'ASX','C' : 'CYS', 'a':'CYS', 'b':'CYS', 'c':'CYS', 'Q' : 'GLN','E' : 'GLU','Z' : 'GLX','G' : 'GLY','H' : 'HIS','I' : 'ILE','L' : 'LEU','K' : 'LYS','M' : 'MET','F' : 'PHE','P' : 'PRO','S' : 'SER','T' : 'THR','W' : 'TRP','Y' : 'TYR','V' : 'VAL','X' : 'PCA'}
def shifty2talos(shiftyLines):
    
    ## parsing shifty formatted chemical shift file
    ## Example:
    #NUM AA HA CA CB CO N HN
    #1 G 3.9965 44.5654  174.5403 109.3458 8.4265
    #2 D 4.6415 54.1507 40.2026 176.5840 121.6465 8.4031
    #3 A 4.1445 55.5122 18.4147 179.2271 126.8915 8.5591
    #4 A 4.1121 54.7719 18.1360 180.3002 119.8723 8.4904
    #5 K 3.9607 58.9846 32.3348 178.6732 118.9015 7.2834
    #6 G 3.8300 47.7694 32.3348 174.8342 108.2192 8.6411
    
    resnum_res_list = []
    resn_shifts = dict() 
    
    for line in shiftyLines:
        if line.startswith("#"):
            ## extracting the atom list from the header
            if line.split()[0] == "#NUM":
            	atomList = line.split()[2:]
            elif line.split()[0] == "#":
                atomList = line.split()[3:]
        if line.strip() and not line.startswith("#"):
            
            elems = line.lstrip().split()
            
            resnum = elems[0]
            residue = resmap[elems[1]]
            resnum_res_list.append(resnum+residue)
            shifts = [0.0] * len(atomList) 
            
            for i in range(2, len(elems)):
                
                shifts[i-2]=float(elems[i])
            
            for j in range (len(atomList)):  
                if atomList[j] == "CO": atomList[j] = "C"    
                resn_shifts[resnum+'_'+residue+'_'+atomList[j]] = shifts[j]
            
    talosLines = ""        
    for i in range (0, len(resnum_res_list), 5):
        s= "SEQ " + " ".join(resnum_res_list[i:i+5])
        l = s.split() 
        if len(l[1:]) < 5:
            for j in range(len(l[1:]), 5):
                l.append(str(0))
            s = " ".join(l)
        talosLines += s+"\n"
    
        
    talosLines += "\nVARS RESID RESNAME ATOMNAME SHIFT\nFORMAT %4d %1s %4s %8.3f\n"  
    for elem in resnum_res_list:
        resnum= elem[:-3]
        residue = elem [-3:]
        
        
        for atom in atomList:
            #print(resn_shifts[resnum+'_'+residue+'_'+atom]) 
            if resn_shifts[resnum+'_'+residue+'_'+atom] != 0.00: 
            	talosLines +="%s %s %s %0.3f\n" % (resnum, residue, atom, resn_shifts[resnum+'_'+residue+'_'+atom])
            
    #print (talosLines)     
    return talosLines        
    
def main():
    shiftyFile = sys.argv[1]
    fin = open(shiftyFile, "r")
    fout = open(shiftyFile.split(".")[0]+".tab", "w")
    shiftyLines = fin.readlines()
    talosLines = shifty2talos(shiftyLines)  
    fout.write(talosLines)
main()    
