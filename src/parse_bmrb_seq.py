#!/usr/local/bin/python3

from collections import OrderedDict
from os import listdir
from os.path import isfile, join
import operator
import os.path
import sys
three2one ={'ALA' : 'A','ARG' : 'R','ASN' : 'N','ASP' : 'D','ASX' : 'B','CYS' : 'C','GLN' : 'Q',
	     'GLU' : 'E','GLX' : 'Z','GLY' : 'G','HIS' : 'H','ILE' : 'I','LEU' : 'L','LYS' : 'K',
	     'MET' : 'M','PHE' : 'F','PRO' : 'P','SER' : 'S','THR' : 'T','TRP' : 'W','TYR' : 'Y',
	     'VAL' : 'V','PCA' : 'X'}



bmrbfile= sys.argv[1]
bmrbfiles=[bmrbfile]
for file in bmrbfiles:
        tmp = file.split(".")
        outfile = tmp[0]+ '.fasta'
        fin = open (file, 'r')
        fout = open (outfile, 'w')
        lines = fin.readlines()
        
        AAstr = ''
        residList = dict()
        residueList = dict()
        
        seqEndFlag = 0
        
	# reading the sequence from the resnum+resiude entries before "VARS" line 
	# Example format:
	#1GLY 2SER 3SER 4GLY 5SER
	#6SER 7GLY 8SER 9LEU 10GLN
	#11THR 12SER 13ASP 14VAL 15VAL

        # OR reading SEQUENCE data from DATA header
        # Example: DATA SEQUENCE TTYKLILNLK QAKEEAIKEA VDAGTAEKYF KLIANAKTVE GVWTYKDEIK
        found = ''
        resn_to_aa = {}
        for i in range(len(lines)):
                line = lines[i].strip()
                if line.startswith("SEQ"):
                        found = '1'
                        #print(line)
                        elems = line.split()
                        for elem in elems:
                               if elem.isalnum() and (elem != "0" and elem!='SEQ'):
                                        AAstr += three2one[elem[-3:]]
                if (found!='1' and line.strip() and line[0].isdigit() and len(line.split()) == 6):
                        elems= line.split()      
                        resn_to_aa[int(elems[0])]= three2one[elems[1]]
	
        resn_to_aa_ordered=OrderedDict(sorted(resn_to_aa.items(), key=operator.itemgetter(0)))
	
        for key, value in resn_to_aa_ordered.items():
                AAstr+=value
	
        fin.close()
        fout.write(">CSI2.0|CHAIN|SEQUENCE\n")
        fout.write(AAstr)
        fout.close()
        
