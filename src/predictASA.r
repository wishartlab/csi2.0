#!/usr/local/bin/Rscript 

library(caret)
library(gbm)





Args <- commandArgs(TRUE)

if (length(Args) < 2) {
  cat("\nError: No input file supplied and scon option are provided!")
  cat("\n\t Syntax: predictASA.R <input-bmrb> <scon-flag>\n\n")
  quit()
}


sconflag= Args[2]

if (sconflag == "sconyes"){
  prediction_model_1res = "/apps/csi/project/Standalone_CSI2.0/models/gbmParamTunedModelScaledGaussianLoss_janinScale_Singleres_jsscon.RData"
  prediction_model_3res = "/apps/csi/project/Standalone_CSI2.0/models/gbmParamTunedModelScaledGaussianLoss_janinScale_3res_jsscon.RData" 
  # loading the 3-residue fetaure GBM Model
  load(prediction_model_3res)
  # loading the single-res feature GBM Model
  load(prediction_model_1res)

  singleResPredModel = ASAPredgbmFitGaussianSingleresjsscon$finalModel
  threeResPredModel = ASAPredgbmFitGaussian3resjsscon$finalModel
  featureLen1res = 8
  featureLen3res = 20
  nTrees1Res = 120
  nTrees3Res = 320
} else if (sconflag == "sconno"){
  prediction_model_1res = "/apps/csi/project/Standalone_CSI2.0/models/gbmParamTunedModelScaledGaussianLoss_janinScale_1res_NoScon.RData"
  prediction_model_3res = "/apps/csi/project/Standalone_CSI2.0/models/gbmParamTunedModelScaledGaussianLoss_janinScale_3res_NoScon.RData" 
  # loading the 3-residue fetaure GBM Model
  load(prediction_model_3res)
  # loading the single-res feature GBM Model
  load(prediction_model_1res)

  singleResPredModel = ASAPredgbmFitGaussian1resNoScon$finalModel
  threeResPredModel = ASAPredgbmFitGaussian3resNoScon$finalModel  
  featureLen1res = 7
  featureLen3res = 17
  nTrees1Res = 80
  nTrees3Res = 160
}  






## single-residue feature prediction
infile = paste(Args[1], "asafeat1res",sep=".")

test.asa_features = read.table(file=infile, sep=' ', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
testData = test.asa_features[,3:featureLen1res]
resnList = test.asa_features[,1]
residueList = test.asa_features[,2]
newdata = data.frame(x=testData)

#Shannon's entropy conservation score
test.data.predict=predict(singleResPredModel, newdata, type="response", n.trees=nTrees1Res)# the last argument indicates number of iterations needed to predict the response

predict=test.data.predict

len = length(predict)
res1class = ifelse(predict[1]>0.25, 'E', 'B')
resLastclass = ifelse(predict[len]>0.25, 'E', 'B')
res1str = paste( resnList[1], residueList[1], predict[1], res1class, sep=" ")
resLaststr = paste( resnList[len], residueList[len], predict[len], resLastclass, sep=" ")


## 3-residue feature prediction
infile = paste(Args[1], "asafeat3res",sep=".")

# naming the output file
outfile = paste(Args[1], "asafeat.pred", sep=".")

test.asa_features = read.table(file=infile, sep=' ', header=TRUE, blank.lines.skip=TRUE, comment.char="#")
testData = test.asa_features[,3:featureLen3res]
resnList = test.asa_features[,1]
residueList = test.asa_features[,2]
newdata = data.frame(x=testData)


test.data.predict=predict(threeResPredModel , newdata, type="response", n.trees=nTrees3Res)# the last argument indicates number of iteration trees needed in the model

predict=test.data.predict

#obs = matrix(observed)
pred = matrix(predict)

print ("after prediction......")

con=file(outfile, "w")
oheader = "#Number Resn Residue PredictedASA Class";
cat(file=con, append=TRUE, oheader, "\n")


# writing the 1st residue prediction
cat(file=con, append=TRUE, res1str, "\n")

class = NULL
for (i in 1:nrow(pred)){
  if (pred[i]>0.25){
    class[i] = 'E'
  }else{
    class[i]='B'
  }
  ostr = paste (resnList[i], residueList[i], pred[i], class[i], sep=" ")
  #save to a file
  cat( file=con, append=TRUE, ostr, "\n" )
}
# writing the last residue prediction
cat(file=con, append=TRUE, resLaststr, "\n")

close(con)
