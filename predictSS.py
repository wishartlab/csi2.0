#!/usr/local/bin/python3
#********************************************
# Secondary Structure prediction main program                             
# - accepts commanl-line options
# Available options:
# a. use Re-referencing
# b. psipred flag
# c. Hidden Marcov model or HMM filtering in 
#    the final prediction
#  
#- output:
#  a. text file with predicted Secondary 
#  structure
#  b. graphical image  
#*********************************************
import os, sys, getopt, re, fnmatch
#from optparse import OptionParser
#from argparse import ArgumentParser
import getopt
import os.path
import subprocess



# global variables
PANAV = "/apps/csi/project/Standalone_CSI2.0/PANAV/PANAV.jar"
BLAST = "/apps/csi/project/softwares/blast-2.2.26"
PSIPRED3 = "/apps/csi/project/softwares/psipred3.5"
RCI = "/apps/csi/project/Standalone_CSI2.0/rci-latest"
RSCRIPT_EXEC = "/usr/bin/Rscript"
SCON_SRC = "/apps/csi/project/Standalone_CSI2.0/conservation_code"
SRC_DIR = "/apps/csi/project/Standalone_CSI2.0/src"


def run_csi2 (options):

    
    

    print("# CSI2.0 Secondary Structure Prediction")
    usage = "usage %prog -i input_bmrb [options]"
    
    
    for option, value in options:
        if option == "-i":
            infile = value
            os.system ("echo %s >> csi2_log" %infile)
        elif option == "-n":
            NRC_FLAG = value 
            os.system ("echo %s >> csi2_log" %NRC_FLAG)
        elif option == "-m":
            FILE_FORMAT = value
        elif option == "-p":
            USE_PSIPRED = value
        elif option == "-f":
            FILTERING = value
        elif option == "-g":
            GRAPHICS = value
        elif option == "-x":
            RCI_FLAG = value
        elif option == "-r":
            REREF = value    
    
    WORKDIR = os.getcwd()
    
    # setting flag options
    if USE_PSIPRED == "y" or USE_PSIPRED == "yes":
        PSIPRED_FLAG = "psipredyes"
    else:
        PSIPRED_FLAG = "psipredno" 

    window_size = ''
    
    if FILTERING == '5res' or FILTERING == '7res':
        FILTER_FLAG = "yes"
        window_size = int(FILTERING[0])
    else:
        FILTER_FLAG = "no"
    
    SCON_FLAG = ""
   
    #REREF = "no"    
    input_bmrb_id = infile.split(".")[0]
    
    #======================================================================
    # pre-processing the data
    #======================================================================
    
    os.system("echo 'Current directory: " + os.getcwd() + "' >> csi2_log")
    
    if FILE_FORMAT == "shifty" :
            REREF = "no"
 
    if REREF == "yes" or REREF == "y" :
        #call PANAV for chemical-shift rereferencing in batch mode
        if FILE_FORMAT == "bmrb2" or FILE_FORMAT == "bmrb3":
            command = "java -jar %s %s %s" %(PANAV, 'str', infile)
            os.system("echo 'running PANAV...' >> csi2_log")
            os.system("echo '" + command + "' >> csi2_log")
            os.system(command)
            
            os.system("echo 'running panav2talos...' >> csi2_log")
            if os.path.exists(infile+".out_calibrated"):
                os.system("mv %s %s" % ((infile+".out_calibrated"), (input_bmrb_id+".out_calibrated")))
                command = "python3 %s/panav2talos.py %s >> csi2_log" %(SRC_DIR, input_bmrb_id)
                os.system("echo '" + command + "' >> csi2_log")
                os.system(command)
            else:
                os.system("echo 'failed to run panav (no file %s found)...running bmrbtotalos converter'>> csi2_log" % (infile+".out_calibrated"))
                os.system("%s/bmrb2talosV2.0.com %s > %s.tab" %(SRC_DIR, infile,input_bmrb_id))                
    else:
        if FILE_FORMAT == "bmrb2" :#bmrb 2.1 format
            os.system("echo 'running bmrb2talos...' >> csi2_log")   
            os.system("%s/bmrb2talosV2.0.com %s > %s.tab" %(SRC_DIR, infile,input_bmrb_id))
        elif FILE_FORMAT == "bmrb3" :#bmrb 3.1 format
            os.system("echo 'running bmrb3toTalos...' >> csi2_log")   
            os.system("python3 %s/bmrb3toTalos.py %s > %s.tab" %(SRC_DIR, infile,input_bmrb_id))    
        elif FILE_FORMAT == "shifty": #shifty format
            # shifty -to- talos conversion and sequence extraction in fasta format
            os.system("echo 'running shifty2talos...' >> csi2_log")
            os.system("python %s/shifty2talos.py %s" %(SRC_DIR, infile))
            
    #to indicate the talos format conversion is done
    os.system("touch donetalos")

                
    # secondary chemical shift calculation with neighbor residue correction
    if NRC_FLAG == "yes" or NRC_FLAG == "no":
        os.system("echo 'running neighbor residue correction...' >> csi2_log")
        os.system("perl %s/seccs_calculation_with_nrc.pl %s.tab %s >> csi2_log" %(SRC_DIR, input_bmrb_id, NRC_FLAG))
        os.system("echo 'mv *.nrc *.tab...' >> csi2_log")
        os.system("mv %s.nrc %s.tab >> csi2_log" %(input_bmrb_id, input_bmrb_id))
    
    #sequence extraction in fasta format
    os.system("echo 'running parse_bmrb_seq ...\n' >> csi2_log")
    os.system("python3 %s/parse_bmrb_seq.py %s.tab >> csi2_log "%(SRC_DIR, input_bmrb_id)) 
   
    # filling up the missing chemical shift entries
    os.system("echo 'running matching triplet and filling the gap in assignment...' >> csi2_log")
    os.system("python3 %s/match_triplet_V2.py %s.tab >> csi2_log " %(SRC_DIR, input_bmrb_id))
    
    #======================================================================
    # feature calculation
    #======================================================================

    # ss_probability
    os.system("echo 'running secondary structure probability...' >> csi2_log")
    os.system("perl %s/ssp_calculation.pl %s.tab >> csi2_log" %(SRC_DIR,input_bmrb_id))

    # Random Coil Index value
    os.chdir(RCI)
    os.system("echo 'in the rci folder...%s' >> %s/csi2_log" %(os.getcwd(),WORKDIR))
    if FILE_FORMAT == "bmrb2": 
        command ="python rci-prog-updated.py -b2 %s/%s" %(WORKDIR, infile)
    if FILE_FORMAT == "bmrb3": 
        command ="python rci-prog-updated.py -b3 %s/%s" %(WORKDIR, infile)    
    elif FILE_FORMAT == "shifty" :#flag = -sh
        command ="python rci-prog-updated.py -sh %s/%s" %(WORKDIR, infile)   
    os.system(command)
    os.chdir(WORKDIR)


    if (not os.path.exists("%s.str.RCI.txt"%input_bmrb_id) and not os.path.exists("%s.str_BMRB.RCI.txt"%input_bmrb_id)):
       os.system("touch errorRCI")
       sys.exit(0)    
    # psipred predicted ss
    if SCON_FLAG == "sconno" and PSIPRED_FLAG == "psipredno":
        PSIPRED_FLAG = "psipredyes"

    if PSIPRED_FLAG == "psipredyes":
        os.system("echo 'running PSIPRED3...' >> csi2_log")
        if not os.path.exists("%s.ss"%input_bmrb_id):
            os.system("%s/runpsipred %s.fasta >> csi2_log" %(PSIPRED3, input_bmrb_id))

        os.system("python3 %s/mapping_psipred_resnumbers.py %s.tab >> csi2_log"%(SRC_DIR, input_bmrb_id) )


    # conservation score
    os.chdir(SCON_SRC)
    os.system("echo 'running conservation score...' >> csi2_log")

    os.system("perl %s/run_conservation_score.pl %s %s %s >> csi2_log" %(SRC_DIR, input_bmrb_id, BLAST, WORKDIR))
    os.chdir(WORKDIR)
    
    if not os.path.exists("%s.scon"%input_bmrb_id):
        SCON_FLAG = "sconno"
    else:
        SCON_FLAG = "sconyes"
    
    # predicted RSA and buried-exposed state
    os.system("echo 'predicting ASA...' >> csi2_log")
    command = "%s/predictASAf.sh %s %s" %(SRC_DIR, input_bmrb_id, SCON_FLAG)
    os.system("echo '" + command + "' >> csi2_log")
    os.system(command)
  
   

    #======================================================================
    # final SS prediction
    #======================================================================
        
    os.system("echo 'creating ss feature file and predicting ss...' >> csi2_log")
   
    command = "%s/predictSS.sh %s %s %s" %(SRC_DIR, input_bmrb_id, PSIPRED_FLAG, SCON_FLAG)
    os.system("echo '" + command + "' >> csi2_log")
    os.system(command)
    

    #======================================================================
    # Markov model filtering
    #======================================================================	
    if FILTER_FLAG == "yes" and window_size:     
        #print(window_size)
        os.system("echo 'running %s window HMM filtering ...' >> csi2_log" % window_size)
        os.system("python3 %s/N-gram_markov_model_filtering.py %s %s > %s.sspred.filt" %(SRC_DIR, input_bmrb_id, window_size, input_bmrb_id))
        os.system("mv %s.sspred.filt %s.sspred1" %(input_bmrb_id,input_bmrb_id))
        
    #======================================================================
    # Output Graphics
    #======================================================================    
    if GRAPHICS == "y" or GRAPHICS == "yes": 
        # output graphics
        os.system("echo 'producing graphics...' >> csi2_log")
        
        command = "%s/output_graphics.sh %s" %(SRC_DIR, input_bmrb_id)
        os.system(command)
        
    final_prediction_file = input_bmrb_id+'.sspred2'     
    if os.path.exists(final_prediction_file):    
        os.system (" echo 'CSI2.0 is successfully done! The predicted secondary structure is in %s' > %s.success" %(final_prediction_file,input_bmrb_id))
    
def main():
    
    
    try:
        #Example: python3 predictSS.py -i bmrb -r no -n yes -m bmrb -p yes -f no -g yes -x no
        opts, args = getopt.getopt(sys.argv[1].split(), 'i:r:n:m:p:f:g:x:')
        
        # parsing arguments for command-line execution
        #opts, args = getopt.getopt(sys.argv[1:], 'i:r:n:m:p:f:g:x:')
        #os.system ("echo %s >> csi2_log" %sys.argv[1:])
        
        
        
        
    except getopt.GetoptError as err:
        os.system("echo %s >> csi2_log" %str(err))
        os.system("echo %s >> csi2_log" %usage)
        sys.exit(2)
   
    run_csi2(opts)
 	 
    	
main()
